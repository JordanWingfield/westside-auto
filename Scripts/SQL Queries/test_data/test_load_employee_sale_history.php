﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "EMPLOYEE_SALE_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$e_result = $conn->query("SELECT SIN FROM Employees");
$s_result = $conn->query("SELECT sale_no FROM Sales");

$sql_employees_arr = array();
$sql_sales_arr = array();

if ($e_result -> num_rows > 0){
	while($row = $e_result -> fetch_assoc()){
		array_push($sql_employees_arr, $row["SIN"]);
	}
} else {
	echo "0 Entries found in EMPLOYEES";
}

if ($s_result -> num_rows > 0){
	while($row = $s_result -> fetch_assoc()){
		array_push($sql_sales_arr, $row["sale_no"]);
	}
} else {
	echo "0 Entries found in SALES";
}

$employees_arr = array($sql_employees_arr[0], $sql_employees_arr[1], $sql_employees_arr[1],
$sql_employees_arr[2], $sql_employees_arr[3], $sql_employees_arr[3], $sql_employees_arr[0],
$sql_employees_arr[4], $sql_employees_arr[0], $sql_employees_arr[4],);

$commission_arr = array(1000, 2000, 2500, 4000, 3525, 1432, 5657, 9875, 8500, 6750);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO employee_sale_history (SIN, sale_no, commission) ";

	$values = "VALUES ('". $employees_arr[$i] ."', '".$sql_sales_arr[$i]."', '". $commission_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
