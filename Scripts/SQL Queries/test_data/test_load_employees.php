﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "EMPLOYEES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:
$sin_arr = array("010-101", "202-020", "303-030", "404-040", "505-050");

$job_arr = array("Salesman", "Salesman", "Buyer", "Buyer", "Salesman");

$first_name_arr = array("Tillie", "Irvin", "Lenny", "Marissa", "Casey");

$last_name_arr = array("Arihm", "Rants", "VanderHoff", "Lowry", "Jenicks");

$phone_no_arr = array("111-222-2222", "111-333-3333", "111-444-4444", "111-555-5555", "111-666-6666");

$salary_arr = array(35000, 40000, 25000, 25000, 50000);

$addr_no_arr = array(40, 45, 124, 67, 198);

$addr_street_arr = array("McMahon St", "St. Abbey Ford Dr", "Lennyville Ave",
"Renfrew Ave", "Rolling Hills Cir");

$addr_city_arr = array("Calgary", "Edmonton", "Edmonton", "Calgary", "Medicine Hat");

$addr_prov_arr = array("AB", "AB", "AB", "AB", "AB");

$addr_postal_code_arr = array("AAA-111", "BBB-222", "CCC-444", "DDD-555", "EEE-666");

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 5;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO Employees (sin, job, first_name, last_name, phone_no, salary, addr_no,
	addr_street, addr_city, addr_province, addr_postal_code) ";

	$values = "VALUES ('". $sin_arr[$i] ."', '". $job_arr[$i] ."', '". $first_name_arr[$i] ."',
	'". $last_name_arr[$i] ."', '". $phone_no_arr[$i] ."', '". $salary_arr[$i] ."',
	'". $addr_no_arr[$i] ."', '". $addr_street_arr[$i] ."', '". $addr_city_arr[$i] ."',
	'". $addr_prov_arr[$i] ."', '". $addr_postal_code_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
