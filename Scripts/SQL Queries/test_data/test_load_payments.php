﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "PAYMENTS";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:
$due_date_arr = array("2014-04-10", "2014-06-11", "2014-06-15", "2014-08-20", "2014-010-30", 
"2015-05-15", "2015-10-28", "2016-08-11", "2016-10-19", "2017-07-09");

$amount_due_arr = array(1000, 1500, 1500, 2000, 1000, 1200, 550, 5500, 1000, 3000);

$paid_date_arr = array("2014-04-08", "2014-06-09", "2014-06-15", "2014-08-19", "2014-010-31", 
"2015-05-13", "2015-10-30", "2016-08-29", "2016-10-20", "2017-07-01");

$amount_paid_arr = array(800, 1200, 1500, 2000, 250, 1100, 550, 4500, 950, 2800);

$account_no_arr = array("AA-0000-000", "BB-1111-111", "AA-0000-000", "CC-2222-222", "DD-3333-333",
"BB-1111-111", "BB-1111-111", "CC-2222-222", "AA-0000-000", "DD-3333-333");

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO Payments (due_date, amount_due, paid_date, amount_paid, account_no) ";

	$values = "VALUES ('". $due_date_arr[$i] ."', '". $amount_due_arr[$i] ."',
	'". $paid_date_arr[$i] ."', '". $amount_paid_arr[$i] ."', '". $account_no_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
