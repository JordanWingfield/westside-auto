﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "WARRANTY_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$w_result = $conn->query("SELECT warranty_no FROM Warranties");
$v_result = $conn->query("SELECT serial_no FROM Vehicles");

$sql_warranties_arr = array();
$sql_vehicles_arr = array();

if ($w_result -> num_rows > 0){
	while($row = $w_result -> fetch_assoc()){
		array_push($sql_warranties_arr, $row["warranty_no"]);
	}
} else {
	echo "0 Entries found in WARRANTIES";
}

if ($v_result -> num_rows > 0){
	while($row = $v_result -> fetch_assoc()){
		array_push($sql_vehicles_arr, $row["serial_no"]);
	}
} else {
	echo "0 Entries found in VEHICLES";
}


$warranties_arr = array($sql_warranties_arr[0], $sql_warranties_arr[1], $sql_warranties_arr[1],
$sql_warranties_arr[2], $sql_warranties_arr[0], $sql_warranties_arr[9], $sql_warranties_arr[7],
$sql_warranties_arr[5], $sql_warranties_arr[5], $sql_warranties_arr[4]);

$vehicles_arr = array($sql_vehicles_arr[0], $sql_vehicles_arr[0], $sql_vehicles_arr[1],
$sql_vehicles_arr[3], $sql_vehicles_arr[3], $sql_vehicles_arr[3], $sql_vehicles_arr[4],
$sql_vehicles_arr[5], $sql_vehicles_arr[7], $sql_vehicles_arr[9]);

$cosigner_f_name_arr = array("John", "Mary", "Markie", "Mackenzie", "Andrew",
"Elizabeth", "Emily", "Tomu", "Harry", "Ryan");

$cosigner_l_name_arr = array("Smith", "Summerland", "Mark", "Aberlin", "Mandrew",
"Queenie", "Robets", "Waraiko", "Potter", "Dendie");


// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO warranty_history (warranty_no, serial_no, cosigner_first_name, 
	cosigner_last_name, total_cost) ";

	$values = "VALUES ('". $warranties_arr[$i] ."', '".$vehicles_arr[$i]."',
	'". $cosigner_f_name_arr[$i] ."', '".$cosigner_l_name_arr[$i]."', '0')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
