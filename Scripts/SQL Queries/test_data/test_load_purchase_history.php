﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "PURCHASE_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$p_result = $conn->query("SELECT purchase_no FROM Purchase");
$v_result = $conn->query("SELECT serial_no FROM Vehicles");

$sql_purchase_arr = array();
$sql_vehicle_arr = array();

if ($p_result -> num_rows > 0){
	while($row = $p_result -> fetch_assoc()){
		array_push($sql_purchase_arr, $row["purchase_no"]);
	}
} else {
	echo "0 Entries found in PURCHASE";
}

if ($v_result -> num_rows > 0){
	while($row = $v_result -> fetch_assoc()){
		array_push($sql_vehicle_arr, $row["serial_no"]);
	}
} else {
	echo "0 Entries found in VEHICLES";
}

$purchase_arr = array($sql_purchase_arr[0], $sql_purchase_arr[1], $sql_purchase_arr[1],
$sql_purchase_arr[2], $sql_purchase_arr[3], $sql_purchase_arr[5], $sql_purchase_arr[5],
$sql_purchase_arr[6], $sql_purchase_arr[7], $sql_purchase_arr[8],);


// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO purchase_history (purchase_no, serial_no) ";

	$values = "VALUES ('". $purchase_arr[$i] ."', '".$sql_vehicle_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
