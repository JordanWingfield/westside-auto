﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "PAYMENT_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$c_result = $conn->query("SELECT customer_id FROM Customers");
$p_result = $conn->query("SELECT payment_no FROM Payments");

$sql_customer_arr = array();
$sql_payment_arr = array();

if ($c_result -> num_rows > 0){
	while($row = $c_result -> fetch_assoc()){
		array_push($sql_customer_arr, $row["customer_id"]);
	}
} else {
	echo "0 Entries found in CUSTOMERS";
}

if ($p_result -> num_rows > 0){
	while($row = $p_result -> fetch_assoc()){
		array_push($sql_payment_arr, $row["payment_no"]);
	}
} else {
	echo "0 Entries found in PAYMENTS";
}


$customer_arr = array($sql_customer_arr[0], $sql_customer_arr[0], $sql_customer_arr[1],
$sql_customer_arr[1], $sql_customer_arr[2], $sql_customer_arr[3], $sql_customer_arr[9],
$sql_customer_arr[9], $sql_customer_arr[8], $sql_customer_arr[5]);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO payment_history (customer_id, payment_no) ";

	$values = "VALUES ('". $customer_arr[$i] ."', '".$sql_payment_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
