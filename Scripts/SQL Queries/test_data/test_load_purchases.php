﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "PURCHASES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:
$date_arr = array("2014-10-22", "2014-12-04", "2014-11-05", "2014-11-06", "2018-06-12",
"2015-01-13", "2015-02-22", "2015-03-14", "2015-08-10", "2015-01-01",
"2015-05-28", "2018-05-16", "2018-01-20", "2017-05-15", "2017-04-13");

$location_arr = array("Calgary", "Lethbridge", "Edmonton", "Banff", "Lethbridge",
"Edmonton", "Edmonton", "Edmonton", "Calgary", "Calgary",
"Edmonton", "Edmonton", "Calgary", "Lethbridge", "Calgary");

$seller_arr = array("Harry Johnson", "Riverside Ford", "Terry Lewis", "Standford Mazda", "Standford Mazda",
"Harry Johnson", "Riverside Ford", "Riverside Ford", "Standford Mazda", "Glenview Toyota",
"Riverside Ford", "Glenview Toyota", "Standford Mazda", "Glenview Toyota", "Terry Lewis");

$auction_arr = array(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 15;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO Purchase (purchase_date, location, seller, bool_is_auction) ";

	$values = "VALUES ('". $date_arr[$i] ."', '". $location_arr[$i] ."',
	'". $seller_arr[$i] ."', '". $auction_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
