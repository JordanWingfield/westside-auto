﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "REPAIRS";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:
$problem_arr = array("Engine exploded", "Transmission fell out", "Clutch does not exist", "It is an automatic", "Weird knocking", "Derpy steering", "It ran out of gas", "Wheel broke", "window smashed in", "Tire was tired");

$est_cost_arr = array(1000,1500,1200,3000,3400,500,2000,2500,2800,2300);

$act_cost_arr = array(978.23,1543.73,1101.01,3356.79,3204.20,499.99,2105.66,2453.43,2963.54,2706.66);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO repairs (problem, estimated_cost, actual_cost) ";

	$values = "VALUES ('". $problem_arr[$i] ."', '". $est_cost_arr[$i] ."',
	'". $act_cost_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
