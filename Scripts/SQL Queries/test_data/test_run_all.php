﻿<?php
//------------------------------------------------------------------------------
// Database PHP test data run-all script:
// Written by: Jordan
// Date: 03-22-18
//------------------------------------------------------------------------------

// Include all creation scripts in order they rely on eachother to create database
include('test_load_customers.php');
include('test_load_employees.php');
include('test_load_employment.php');
include('test_load_purchases.php');
include('test_load_payments.php');
include('test_load_repairs.php');
include('test_load_sales.php');
include('test_load_vehicles.php');
include('test_load_warranties.php');
include('test_load_warranty_history.php');
include('test_load_repair_history.php');
include('test_load_employment_history.php');
include('test_load_payment_history.php');
include('test_load_purchase_history.php');
include('test_load_customer_sale_history.php');
include('test_load_vehicle_sale_history.php');
include('test_load_employee_sale_history.php');

?>
