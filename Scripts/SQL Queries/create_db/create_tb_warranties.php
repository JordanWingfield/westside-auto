<?php
//-----------------------------------------------------
// Table creation PHP script
// Written By: Chris (Via Jordan and W3Schools)
// 03-22-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "WARRANTIES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Declare SQL query to run: In this case we are creating a table.
$sql = "CREATE TABLE warranties (warranty_no int AUTO_INCREMENT,
warranty_name varchar(255),
start_date date,
deductable real,
cost real,
coverage_description varchar(255),
PRIMARY KEY (warranty_no))";

// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
if ($conn->query($sql) === TRUE) {
  echo "Table " . $tbname . " created successfully.";
} else {
  echo "Error creating table " . $tbname . ": " . $conn->error;
}
echo "<br>";

$conn->close();
?>
