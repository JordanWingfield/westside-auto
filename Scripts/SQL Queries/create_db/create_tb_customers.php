﻿<?php
//-----------------------------------------------------
// Table creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-22-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "CUSTOMERS";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Declare SQL query to run: In this case we are creating a table.
$sql = "CREATE TABLE Customers (customer_id int AUTO_INCREMENT,
license_no varchar(255),
customer_SIN varchar(255),
first_name varchar(255),
last_name varchar(255),
phone_no varchar(255),
addr_street_no int,
addr_street varchar(255),
addr_city varchar(255),
addr_province varchar(255),
addr_postal_code varchar(255),
PRIMARY KEY (customer_id))";

// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
if ($conn->query($sql) === TRUE) {
  echo "Table " . $tbname . " created successfully.";
} else {
  echo "Error creating table " . $tbname . ": " . $conn->error;
}
echo "<br>";

$conn->close();
?>
