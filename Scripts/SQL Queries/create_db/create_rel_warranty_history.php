<?php
//-----------------------------------------------------
// Table creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-22-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "WARRANTY_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Declare SQL query to run: In this case we are creating a table.
$sql = "CREATE TABLE Warranty_history (warranty_no int,
serial_no int,
cosigner_first_name varchar(255),
cosigner_last_name varchar(255),
total_cost int,
PRIMARY KEY (warranty_no, serial_no),
FOREIGN KEY (serial_no) REFERENCES VEHICLES (serial_no),
FOREIGN KEY (warranty_no) REFERENCES WARRANTIES (warranty_no))";

// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
if ($conn->query($sql) === TRUE) {
  echo "Table " . $tbname . " created successfully.";
} else {
  echo "Error creating table " . $tbname . ": " . $conn->error;
}
echo "<br>";

$conn->close();
?>