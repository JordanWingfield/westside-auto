﻿<?php
//------------------------------------------------------------------------------
// Database PHP run-all script:
// Written by: Jordan
// Date: 03-22-18
//------------------------------------------------------------------------------

// Clean all
include('clean_all.php');

// Include all creation scripts in order they rely on eachother to create database
include('create_tb_customers.php');
include('create_tb_employees.php');
include('create_tb_employment.php');
include('create_tb_payments.php');
include('create_tb_purchase.php');
include('create_tb_repairs.php');
include('create_tb_sales.php');
include('create_tb_vehicles.php');
include('create_tb_warranties.php');

include('create_rel_employment_history.php');
include('create_rel_payment_history.php');
include('create_rel_purchase_history.php');
include('create_rel_repair_history.php');
include('create_rel_warranty_history.php');
include('create_rel_customer_sale_history.php');
include('create_rel_employee_sale_history.php');
include('create_rel_vehicle_sale_history.php');

include('../test_data/test_run_all.php');

?>
