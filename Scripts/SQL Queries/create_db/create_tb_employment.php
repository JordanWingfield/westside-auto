﻿<?php
//-----------------------------------------------------
// Table creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-22-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "EMPLOYMENT";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Declare SQL query to run: In this case we are creating a table.
$sql = "CREATE TABLE Employment (employment_id int AUTO_INCREMENT,
start_date date,
employer varchar(255),
title varchar(255),
supervisor varchar(255),
phone_no varchar(255),
addr varchar(255),
PRIMARY KEY (employment_id))";

// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
if ($conn->query($sql) === TRUE) {
  echo "Table " . $tbname . " created successfully.";
} else {
  echo "Error creating table " . $tbname . ": " . $conn->error;
}
echo "<br>";

$conn->close();
?>
