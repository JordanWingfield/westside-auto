﻿<?php
//-----------------------------------------------------
// Database Drop-All (Clean) Command
// Adapted From: https://stackoverflow.com/questions/3493253/how-to-drop-all-tables-in-database-without-dropping-the-database-itself
// 03-23-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Disable foreign key checking
$conn->query('SET foreign_key_checks = 0');

// Drop all tables in DB
if ($result = $conn->query("SHOW TABLES"))
{
    while($row = $result->fetch_array(MYSQLI_NUM))
    {
        $conn->query('DROP TABLE IF EXISTS '.$row[0]);
    }
}

echo "===========================================<br>";
echo "   Database Cleaned: All tabled dropped.<br>";
echo "-------------------------------------------<br>";

// Enable foreign key checking
$conn->query('SET foreign_key_checks = 1');

$conn->close();
?>
