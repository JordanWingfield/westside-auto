<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="Styles/stylesheet.css">
	<title>West Side Auto</title>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
		<div class="main_grid">
		<style>
        table, td {
            border: 1px solid black;
        }
        th {
            border: 1px solid black;
            font-size: large;
        }
        </style>
		<?php
					
					// Connect to database:
					$servername = "localhost";
					$username = "root";
					$password = "";
					$dbname = "westside_auto";

					$conn = new mysqli($servername, $username, $password, $dbname);
					$total_cost = "";

					if ($conn -> connect_error){
						die("Unable to connect to database at this time: Please try again later, or contact support.");
					}
					$repair = "SELECT * FROM repairs";
					$repair_result = $conn -> query($repair);
		?>
					<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
            		<fieldset>
           			<legend>Repair Report</legend>
					Please enter VehicleID:
					<input type="text" name="vehicleid">
					<input type="submit" value="Submit">
					</fieldset>
					</form>
		<?php
					if($_SERVER["REQUEST_METHOD"] == "POST"){
;
						$vehicleid = $_POST['vehicleid'];

						$sql_query = "
						SELECT * 
						FROM Vehicles AS v, Repair_History AS rh, Repairs AS r
						WHERE (v.serial_no = '". $vehicleid ."') AND (v.serial_no = rh.serial_no) AND (rh.repair_id = r.repair_id)";

						$raw_results = $conn -> query($sql_query);
							if($raw_results != FALSE && mysqli_num_rows($raw_results) > 0){
							echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
									echo "<th>Miles</th>";
									echo "<th>Repair</th>";
									echo "<th>Estimated Cost</th>";
									echo "<th>Actual Cost</th>";
								echo "</tr>";
								while($results = mysqli_fetch_array($raw_results)){
									echo "<tr>";
										echo "<td>".$results['serial_no']."</td>";
										echo "<td>".$results['make']."</td>";
										echo "<td>".$results['model']."</td>";
										echo "<td>".$results['year']."</td>";
										echo "<td>".$results['miles']."</td>";
										echo "<td>".$results['problem']."</td>";
										echo "<td>".$results['estimated_cost']."</td>";
										echo "<td>".$results['actual_cost']."</td>";
										$total_cost = $results['actual_cost'] + $total_cost;
									echo "</tr>";
									}
								echo "</table>";
								echo "<br>";
								echo "<table>";
								echo "<tr>";
								echo "<th>Total Cost</t>";
								echo "<td>$".$total_cost."</td>";
								echo "</table>";
								echo "<br>";
							} 	else {
								echo "<br>";
								echo "No Vehicle Found, Try Again";
								echo "<br>";
							}
					}
		?>

		</div>
	</div>
</body>
</html>