<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="Styles/stylesheet.css">
	<title>West Side Auto</title>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
		<div class="main_grid">
			<br>
			<b>Welcome to the Westside Auto Database System:</b>
			<br><br>
			Please begin my making your selection on the left-hand navigation pane.
			<br>
		</div>
	</div>
</body>
</html>