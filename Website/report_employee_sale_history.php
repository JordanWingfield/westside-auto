<!-------------------------------------------------------------------------------------- 
Westside Auto: Sale Form Page 
Created by: J. Wingfield
Date: 04-04-18

This page is used for the purpose of adding a new sale,
to the database of WESTSIDE AUTO, including vehicle to sell, customer to sell to,
employee (or salesperson), and customer employment history.
---------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="Styles/stylesheet.css">
    <title>West Side Auto</title>
    <style>
        .error {
            color: #FF0000;
        }	
        table {
            border: 1px solid black;
            font-size: large;
        }
    </style>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
        <div class="main_grid">
			<?php
				// Connect to database:
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "westside_auto";

				$conn = new mysqli($servername, $username, $password, $dbname);

				if ($conn -> connect_error){
					die("Unable to connect to database at this time: Please try again later, or contact support.");
				}

				$b_can_send = FALSE;
				$e_sin = "";

				if ($_SERVER["REQUEST_METHOD"] == "POST"){
					if (!empty($_POST['query'])){
						$b_can_send = TRUE;
						$e_sin = $_POST['query'];
					}
				}

			?>

			
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
				<h2>Sale look-up form:</h2>
				
				Employee SIN: <input type="text" name="query" value="<?php echo $e_sin ?>">
				<input type="submit" text="Search" value="search">

			</form>

			<br><br><br>
			<!-- QUERY SECTION -->
			<!---------------------------------------------------------------------------------------------------
				This section will hold all information related to the queries, including a check for all REQUIRED 
				variables before sending data to the database. 
			---------------------------------------------------------------------------------------------------->
			<?php
				if($b_can_send ==TRUE){
				
					$sql_employee_info = "SELECT * FROM Employees WHERE SIN = '" . $e_sin . "'";
					
					$result = $conn -> query($sql_employee_info);

					if ($result != FALSE && $result -> num_rows > 0){
						$row = $result -> fetch_assoc();
						echo "<h2>". $row['first_name'] . " ". $row['last_name'] ."</h2><br>";
						echo "<table>";
						echo "<tr>
								<td colspan = '4'>
									<b>Employee information</b>
									<br>
								</td>
							</tr>";
						echo "<tr>
								<td>
									<br>
									SIN: <br>
									Position: <br>
									Phone Number: <br>
									Salary: <br>
									<br>
								</td>
								<td>
									<br>
									". $row['SIN'] . "<br>
									". $row['job'] . "<br>
									". $row['phone_no'] . "<br>
									". $row['salary'] . "
									<br>
								</td>
								<td>
									Address:<br>
								</td>
								<td>
									". $row['addr_no'] . " 
									". $row['addr_street'] . " 
									". $row['addr_city'] . " 
									". $row['addr_province'] . " 
									". $row['addr_postal_code'] . " 
								</td>
							</tr>";
						echo "</table><br>";
					}

					$sql_sale_history = "
					SELECT s.sale_date, c.first_name, c.last_name, v.serial_no, v.make, v.model, es.commission, s.total_due
					FROM sales AS s, employees AS e, employee_sale_history AS es, customers AS c, customer_sale_history AS cs, vehicles AS v, vehicle_sale_history AS vs
					WHERE s.sale_no = es.sale_no AND e.SIN = es.SIN AND e.SIN =  '". $e_sin ."' AND c.customer_id = cs.customer_id AND v.serial_no = vs.serial_no AND cs.sale_no = s.sale_no AND vs.sale_no = s.sale_no";

					$result = $conn -> query($sql_sale_history);

					if ($result != FALSE && $result -> num_rows > 0){
						echo "<table>";
						echo "<tr>
								<td colspan = '8'>	
									<b>Sale Records</b>
								</td>
							</tr>";

						echo "<tr>
								<td>
									<i>Date</i>
								</td>
								<td colspan ='2'>
									<i>Customer</i>
								</td>
								<td colspan = '3'>
									<i>Vehicle</i>
								</td>
								<td>
									<i>Commission</i>
								</td>
								<td>
									<i>Total</i>
								</td>
							</tr>";

						while($row = $result -> fetch_assoc()){
							echo "<tr>
									<td>
										". $row['sale_date'] . " &emsp;    
									</td>
									<td>
										". $row['first_name'] . " &emsp;   
									</td>
									<td>
										". $row['last_name'] . " &emsp;   
									</td>
									<td>
										". $row['serial_no'] . " &emsp;   
									</td>
									<td>
										". $row['make'] . " &emsp;  
									</td>
									<td>
										". $row['model'] . " &emsp;   
									</td>
									<td>
										". $row['commission'] . " &emsp;   
									</td>
									<td>
										". $row['total_due'] . " &emsp;   
									</td>
								</tr>";
						}

						echo "</table>";
					} 

				}
				$conn -> close();	
			?>
			<br><br><br>
        </div>
	</div>
</body>
</html>