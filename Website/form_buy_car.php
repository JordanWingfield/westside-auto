<!-------------------------------------------------------------------------------------- 
Westside Auto: Purchase Form Page 
Created by: J. Wingfield
Date: 04-02-18

This page is used for the purpose of adding a new vehicle with repair history
to the database of WESTSIDE AUTO. 
---------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="Styles/stylesheet.css">
    <title>West Side Auto</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
        <div class="main_grid">
			<?php

				$date = $location = $b_auction = $dealer = $style = $ext_colour = $int_colour = "";
				$make = $model = $year = $serial_no = $miles = $condition = $b_price = $p_price = "";
				$v_desc_1 = $v_desc_2 = $v_desc_3 = $v_est_cost_1 = $v_est_cost_2 = $v_est_cost_3 = $v_act_cost_1 = $v_act_cost_2 = $v_act_cost_3 = "";

				$problem_1_count = 0;
				$problem_2_count = 0;
				$problem_3_count = 0;


				$can_send = FALSE;
				$repeating_send = FALSE;

				if ($_SERVER["REQUEST_METHOD"] == "POST") {

					$can_send = TRUE;

					if (!empty($_POST["date"])){
						$date = test_input($_POST["date"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["location"])){
						$location = test_input($_POST["location"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["b_auction"])){
						$b_auction = test_input($_POST["b_auction"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["dealer"])){
						$dealer = test_input($_POST["dealer"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["make"])){
						$make = test_input($_POST["make"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["model"])){
						$model = test_input($_POST["model"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["year"])){
						$year = test_input($_POST["year"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["style"])){
						$style = test_input($_POST["style"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["ext_colour"])){
						$ext_colour = test_input($_POST["ext_colour"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["int_colour"])){
						$int_colour = test_input($_POST["int_colour"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["serial_no"])){
						$serial_no = test_input($_POST["serial_no"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["miles"])){
						$miles = test_input($_POST["miles"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["condition"])){
						$condition = test_input($_POST["condition"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["b_price"])){
						$b_price = test_input($_POST["b_price"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["p_price"])){
						$p_price = test_input($_POST["p_price"]);
					} else {
						$can_send = FALSE;
					}

					if (!empty($_POST["v_desc_1"])){
						$v_desc_1 = test_input($_POST["v_desc_1"]);
						$problem_1_count += 1;
					} 

					if (!empty($_POST["v_est_cost_1"])){
						$v_est_cost_1 = test_input($_POST["v_est_cost_1"]);
						$problem_1_count += 1;
					}
					
					if (!empty($_POST["v_act_cost_1"])){
						$v_act_cost_1 = test_input($_POST["v_act_cost_1"]);
						$problem_1_count += 1;
					}

					if (!empty($_POST["v_desc_2"])){
						$v_desc_2 = test_input($_POST["v_desc_2"]);
						$problem_2_count += 1;
					}

					if (!empty($_POST["v_est_cost_2"])){
						$v_est_cost_2 = test_input($_POST["v_est_cost_2"]);
						$problem_2_count += 1;
					}

					if (!empty($_POST["v_act_cost_2"])){
						$v_act_cost_2 = test_input($_POST["v_act_cost_2"]);
						$problem_2_count += 1;
					}

					if (!empty($_POST["v_desc_3"])){
						$v_desc_3 = test_input($_POST["v_desc_3"]);
						$problem_3_count += 1;
					}

					if (!empty($_POST["v_est_cost_3"])){
						$v_est_cost_3 = test_input($_POST["v_est_cost_3"]);
						$problem_3_count += 1;
					}

					if (!empty($_POST["v_act_cost_3"])){
						$v_act_cost_3 = test_input($_POST["v_act_cost_3"]);
						$problem_3_count += 1;
					}

				}

				function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
				}
			?>

			
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
			<h2>Vehicle Purchase Form:</h2>
			<table>
				<tr>
					<td>
						<p><span class="error">* required field.</span></p> 
							Date: <input type="date" name="date" value="<?php echo $date;?>">
							<span class="error">*</span>
							<br><br>

							Location: <input type="text" name="location" value="<?php echo $location;?>">
							<span class="error">*</span>
							<br><br>

							Auction:
							<input type="radio" name="b_auction" <?php if (isset($b_auction) && $b_auction=="yes") echo "checked";?> value="yes">Yes
							<input type="radio" name="b_auction" <?php if (isset($b_auction) && $b_auction=="no") echo "checked";?> value="no">No
							<span class="error">*</span>
							<br><br>

							Seller/Dealer: <input type="text" name="dealer" value="<?php echo $dealer;?>">
							<span class="error">*</span>
							<br><br>

							<input type="submit" name="submit" value="Submit"> 
					</td>
					<td>
						<h3> Vehicle Information: </h3>
						Make: <input type="text" name="make" value="<?php echo $make;?>">
						<span class="error">*</span>
				
						Model: <input type="text" name="model" value="<?php echo $model;?>">
						<span class="error">*</span>
						<br><br>

						Serial Number: <input type="text" name="serial_no" value="<?php echo $serial_no;?>">
						<span class="error">*</span>
				
						Style: <input type="text" name="style" value="<?php echo $style;?>">
						<span class="error">*</span>

						<br><br>

						Year: <input type="text" name="year" value="<?php echo $year;?>">
						<span class="error">*</span>

						Miles: <input type="text" name="miles" value="<?php echo $miles;?>">
						<span class="error">*</span>
						<br><br>

						Condition: <input type="text" name="condition" value="<?php echo $condition;?>">
						<span class="error">*</span>
						<br><br>

						Exterior Colour: <input type="text" name="ext_colour" value="<?php echo $ext_colour;?>">
						<span class="error">*</span>

						Interior Colour: <input type="text" name="int_colour" value="<?php echo $int_colour;?>">
						<span class="error">*</span>

						<br><br>

						Book Price: <input type="text" name="b_price" value="<?php echo $b_price;?>">
						<span class="error">*</span>

						Price Paid: <input type="text" name="p_price" value="<?php echo $p_price;?>">
						<span class="error">*</span>
						<br><br>
					</td>
				</tr>
				<tr>
					<td/>
					<td>
						<h4> Problems: </h4>
						<table>
						<tr>
							<td/>
							<td>Description</td>
							<td>Est. Repair Cost</td>
							<td>Actual Repair Cost</td>
						</tr>
						<tr>
							<td>
							1.
							</td>
							<td>
								<input type = "text" name = "v_desc_1" value = "<?php echo $v_desc_1;?>">
							</td>
							<td>
								<input type = "text" name = "v_est_cost_1" value = "<?php echo $v_est_cost_1;?>">
							</td>
							<td>
								<input type = "text" name = "v_act_cost_1" value = "<?php echo $v_act_cost_1;?>">
							</td>
						</tr>
						<tr>
							<td>
							2.
							</td>
							<td>
								<input type = "text" name = "v_desc_2" value = "<?php echo $v_desc_2;?>">
							</td>
							<td>
								<input type = "text" name = "v_est_cost_2" value = "<?php echo $v_est_cost_2;?>">
							</td>
							<td>
								<input type = "text" name = "v_act_cost_2" value = "<?php echo $v_act_cost_2;?>">
							</td>
						</tr>
						<tr>
							<td>
							3.
							</td>
							<td>
								<input type = "text" name = "v_desc_3" value = "<?php echo $v_desc_3;?>">
							</td>
							<td>
								<input type = "text" name = "v_est_cost_3" value = "<?php echo $v_est_cost_3;?>">
							</td>
							<td>
								<input type = "text" name = "v_act_cost_3" value = "<?php echo $v_act_cost_3;?>">
							</td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
 
			</form>

			<!-- QUERY SECTION-->
			<!---------------------------------------------------------------------------------------------------
				This section will hold all information related to the queries, including a check for all REQUIRED 
				variables before sending data to the database. 
			---------------------------------------------------------------------------------------------------->
			<?php
				if ($can_send == TRUE && $repeating_send == FALSE){
				
					// Connect to database:
					$servername = "localhost";
					$username = "root";
					$password = "";
					$dbname = "westside_auto";

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn -> connect_error){
						die("Unable to connect to database at this time: Please try again later, or contact support.");
					}

					// Create query to insert into vehicles
					
					$sql_vehicle = "INSERT INTO Vehicles (serial_no, make, model, 
					year, style, miles, ext_colour, int_colour, vehicle_condition, 
					book_price, buy_price) ";
					$sql_vehicle = $sql_vehicle . "VALUES ('". $serial_no ."', '". $make ."',
					'". $model ."', '". $year ."', '". $style ."', '". $miles ."',
					'". $ext_colour ."', '". $int_colour ."', '". $condition ."', '". $b_price ."', '". $p_price ."')";
					
					if ($conn -> query($sql_vehicle) === FALSE){
						die("<br><span class='error'>Error in submitting: This vehicle has already been purchased.</span><br>");
					}

					$vehicle_id = $serial_no;
					
					// Create query to insert into purchase
					$sql_purchase = "INSERT INTO Purchase (purchase_date, location, seller, bool_is_auction) ";
					$sql_purchase = $sql_purchase . "VALUES ('". $date ."', '". $location ."', '". $dealer ."', '"; 
					if ($b_auction == "yes"){
						$sql_purchase = $sql_purchase . "1')";
					} else {
						$sql_purchase = $sql_purchase . "0')";
					}


					$purchase_id = 0;

					if ($conn -> query($sql_purchase) === TRUE){
						$purchase_id = $conn -> insert_id;
					}
					
					// Create query to insert into purchase_history
					$sql_purchase_history = "INSERT INTO Purchase_History (purchase_no, serial_no) 
					VALUES ('". $purchase_id ."', '". $vehicle_id ."')";

					$conn -> query($sql_purchase_history);

					// Create query to insert into repairs
					if ($problem_1_count > 0){
					
						if ($v_desc_1 == "") {$v_desc_1 = "NULL";}
						if ($v_est_cost_1 == "") {$v_est_cost_1 = "NULL";}
						if ($v_act_cost_1 == "") {$v_act_cost_1 = "NULL";}

						$sql_repair = "INSERT INTO Repairs (problem, estimated_cost, actual_cost) ";
						$sql_repair = $sql_repair . "VALUES ('". $v_desc_1 ."',
						'". $v_est_cost_1 ."','". $v_act_cost_1 ."')";

						$conn -> query($sql_repair);
						$repair_id = $conn -> insert_id;

						// Create query to insert into repair history
						$sql_repair_history = "INSERT INTO Repair_History (serial_no, repair_id) ";
						$sql_repair_history = $sql_repair_history . "VALUES ('". $vehicle_id ."',
						'". $repair_id ."')";

						$conn -> query($sql_repair_history);
					}

					// Create query to insert into repairs
					if ($problem_2_count > 0){
					
						if ($v_desc_2 == "") {$v_desc_2 = "NULL";}
						if ($v_est_cost_2 == "") {$v_est_cost_2 = "NULL";}
						if ($v_act_cost_2 == "") {$v_act_cost_2 = "NULL";}

						$sql_repair = "INSERT INTO Repairs (problem, estimated_cost, actual_cost) ";
						$sql_repair = $sql_repair . "VALUES ('". $v_desc_2 ."',
						'". $v_est_cost_2 ."','". $v_act_cost_2 ."')";

						$conn -> query($sql_repair);
						$repair_id = $conn -> insert_id;

						// Create query to insert into repair history
						$sql_repair_history = "INSERT INTO Repair_History (serial_no, repair_id) ";
						$sql_repair_history = $sql_repair_history . "VALUES ('". $vehicle_id ."',
						'". $repair_id ."')";

						$conn -> query($sql_repair_history);
					}

					// Create query to insert into repairs
					if ($problem_3_count > 0){
					
						if ($v_desc_3 == "") {$v_desc_3 = "NULL";}
						if ($v_est_cost_3 == "") {$v_est_cost_3 = "NULL";}
						if ($v_act_cost_3 == "") {$v_act_cost_3 = "NULL";}

						$sql_repair = "INSERT INTO Repairs (problem, estimated_cost, actual_cost) ";
						$sql_repair = $sql_repair . "VALUES ('". $v_desc_3 ."',
						'". $v_est_cost_3 ."','". $v_act_cost_3 ."')";

						$conn -> query($sql_repair);
						$repair_id = $conn -> insert_id;

						// Create query to insert into repair history
						$sql_repair_history = "INSERT INTO Repair_History (serial_no, repair_id) ";
						$sql_repair_history = $sql_repair_history . "VALUES ('". $vehicle_id ."',
						'". $repair_id ."')";

						$conn -> query($sql_repair_history);
					}
					$conn -> close();
					
					echo "<br><h3>Purchase processed: Thank you.</h3><br>";
					$repeating_send = TRUE;

				} elseif ($_SERVER["REQUEST_METHOD"] == "POST"){
					
					echo "<br><span class='error'>Please populate all required fields before submitting.</span><br>";

				}	
			?>

        </div>
	</div>
</body>
</html>