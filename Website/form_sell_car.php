<!-------------------------------------------------------------------------------------- 
Westside Auto: Sale Form Page 
Created by: J. Wingfield
Date: 04-04-18

This page is used for the purpose of adding a new sale,
to the database of WESTSIDE AUTO, including vehicle to sell, customer to sell to,
employee (or salesperson), and customer employment history.
---------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="Styles/stylesheet.css">
    <title>West Side Auto</title>
    <style>
        .error {
            color: #FF0000;
        }	
    </style>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
        <div class="main_grid">
			<?php
				// Connect to database:
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "westside_auto";

				$conn = new mysqli($servername, $username, $password, $dbname);

				if ($conn -> connect_error){
					die("Unable to connect to database at this time: Please try again later, or contact support.");
				}

				$can_send = FALSE;

				$var_names = array("date" => "", "total_due" => "", "down_payment" => "", "finance_amount" => "",
				"e_last_name" => "", "e_first_name" => "", "e_sin" => "", "e_phone_no" => "", "e_commission" => "",
				"serial_no" => "", "make" => "", "model" => "", "year" => "", "style" => "", "miles" => "", "ext_colour" => "", "int_colour" => "", "condition" => "", "b_price" => "",
				"c_last_name" => "", "c_first_name" => "", "c_phone_no" => "", "c_license_no" => "", "c_sin" => "", "c_street_no" => "", "c_street_name" => "", "c_city" => "", "c_province" => "", "c_postal_code" => "",
				"h_employer_1" => "", "h_title_1" => "", "h_supervisor_1" => "", "h_phone_no_1" => "", "h_address_1" => "", "h_start_date_1" => "",
				"h_employer_2" => "", "h_title_2" => "", "h_supervisor_2" => "", "h_phone_no_2" => "", "h_address_2" => "", "h_start_date_2" => "",
				"h_employer_3" => "", "h_title_3" => "", "h_supervisor_3" => "", "h_phone_no_3" => "", "h_address_3" => "", "h_start_date_3" => "",
				"h_employer_4" => "", "h_title_4" => "", "h_supervisor_4" => "", "h_phone_no_4" => "", "h_address_4" => "", "h_start_date_4" => "",
				"h_employer_5" => "", "h_title_5" => "", "h_supervisor_5" => "", "h_phone_no_5" => "", "h_address_5" => "", "h_start_date_5" => "");

				
				$e1_string_arr = array("h_employer_1", "h_title_1", "h_supervisor_1", "h_phone_no_1", "h_address_1");
				$e2_string_arr = array("h_employer_2", "h_title_2", "h_supervisor_2", "h_phone_no_2", "h_address_2");
				$e3_string_arr = array("h_employer_3", "h_title_3", "h_supervisor_3", "h_phone_no_3", "h_address_3");
				$e4_string_arr = array("h_employer_4", "h_title_4", "h_supervisor_4", "h_phone_no_4", "h_address_4");
				$e5_string_arr = array("h_employer_5", "h_title_5", "h_supervisor_5", "h_phone_no_5", "h_address_5");
				$employment_hist_count_arr = array("h_1" => 0, "h_2" => 0, "h_3" => 0, "h_4" => 0, "h_5" => 0);
				

				if ($_SERVER["REQUEST_METHOD"] == "POST") {

					$can_send = TRUE;

					foreach ($var_names as $key => $value){
				
						if (!empty($_POST[$key])){
							// Maintain constant values for all keys recorded.
							$var_names[$key] = test_input($_POST[$key]);

							// Check number of fields (optional) entered for employment history
							if (in_array($key, $e1_string_arr)){
								$employment_hist_count_arr["h_1"] += 1;
							} elseif (in_array($key, $e2_string_arr)){
								$employment_hist_count_arr["h_2"] += 1;
							} elseif (in_array($key, $e3_string_arr)){
								$employment_hist_count_arr["h_3"] += 1;
							} elseif (in_array($key, $e4_string_arr)){
								$employment_hist_count_arr["h_4"] += 1;
							} elseif (in_array($key, $e5_string_arr)){
								$employment_hist_count_arr["h_5"] += 1;
							} 
						} elseif (!in_array($key, $e1_string_arr) && !in_array($key, $e2_string_arr) && !in_array($key, $e3_string_arr) && 
						!in_array($key, $e4_string_arr) && !in_array($key, $e5_string_arr) && ($key != 'h_start_date_1') && ($key != 'h_start_date_2') &&
						($key != 'h_start_date_3') && ($key != 'h_start_date_4') && ($key != 'h_start_date_5')) {
							$can_send = FALSE;
						} 
					}
				}

				$sql_get_vehicle_info = "SELECT * FROM Vehicles WHERE serial_no = ". $var_names['serial_no'];
				$sql_get_employee_info = "SELECT * FROM Employees WHERE SIN = '". $var_names['e_sin'] ."'";
				$sql_get_customer_info = "SELECT * FROM Customers WHERE customer_SIN = " . $var_names['c_sin'];


				$result = $conn -> query($sql_get_vehicle_info);
				if ($result != FALSE && $result -> num_rows > 0){
					$row = $result -> fetch_assoc();
					$var_names['serial_no'] = $row['serial_no'];
					$var_names['make'] = $row['make'];
					$var_names['model'] = $row['model'];
					$var_names['year'] = $row['year'];
					$var_names['style'] = $row['style'];
					$var_names['miles'] = $row['miles'];
					$var_names['ext_colour'] = $row['ext_colour'];
					$var_names['int_colour'] = $row['int_colour'];
					$var_names['condition'] = $row['vehicle_condition'];
					$var_names['b_price'] = $row['book_price'];
				} else {
					$can_send = FALSE;
				}
				
				$result = $conn -> query($sql_get_employee_info);
				if ($result != FALSE && $result -> num_rows > 0){
					$row = $result -> fetch_assoc();
					$var_names['e_last_name'] = $row['last_name'];
					$var_names['e_first_name'] = $row['first_name'];
					$var_names['e_sin'] = $row['SIN'];
					$var_names['e_phone_no'] = $row['phone_no'];
				} else {
					$can_send = FALSE;
				}

				$result = $conn -> query($sql_get_customer_info);
				if ($result != FALSE && $result -> num_rows > 0){
					$row = $result -> fetch_assoc();
					$var_names['c_last_name'] = $row['last_name'];
					$var_names['c_first_name'] = $row['first_name'];
					$var_names['c_phone_no'] = $row['phone_no'];
					$var_names['c_license_no'] = $row['license_no'];
					$var_names['c_sin'] = $row['customer_SIN'];
					$var_names['c_street_no'] = $row['addr_street_no'];
					$var_names['c_street_name'] = $row['addr_street'];
					$var_names['c_city'] = $row['addr_city'];
					$var_names['c_province'] = $row['addr_province'];
					$var_names['c_postal_code'] = $row['addr_postal_code'];
				} else {
				}
			
				function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
				}
			?>

			
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
			<h2>Vehicle Sale Form:</h2>
			<table>
				<!-- SALE information and SALESPERSON-->
				<tr>
					<!-- Sale table-->
					<td>
						<table>
							<tr>
								<th colspan = "2"> <h3>Sale</h3> </th>
							</tr>
							<tr>
								<td>
									<span class="error">*</span>Date <br>
									<span class="error">*</span>Total Due <br>
									<span class="error">*</span>Down Payment <br>
									<span class="error">*</span>Finance Amount <br>
								</td>
								<td>
									<input type="date" name="date" value="<?php echo $var_names['date']; ?>"><br> 
									<input type="text" name="total_due" value="<?php echo $var_names['total_due']; ?>"><br>
									<input type="text" name="down_payment" value="<?php echo $var_names['down_payment']; ?>"><br>
									<input type="text" name="finance_amount" value="<?php echo $var_names['finance_amount']; ?>"><br>
								</td>
							</tr>
						</table>
					</td>

					<!-- Salesperson table-->
					<td>
						<table>
							<tr>
								<th colspan = "2"> <h3> Salesperson </h3> </th>
							</tr>
							<tr>
								<td>
									<span class="error">*</span><b>SIN</b> <br>
									<span class="error">*</span>Last Name <br>
									<span class="error">*</span>First Name <br>
									<span class="error">*</span>Phone Number <br>
									<span class="error">*</span>Commission <br>
								</td>
								<td>
									<input type="text" name="e_sin" value="<?php echo $var_names['e_sin']; ?>"><br>
									<input type="text" name="e_last_name" value="<?php echo $var_names['e_last_name']; ?>"><br>
									<input type="text" name="e_first_name" value="<?php echo $var_names['e_first_name']; ?>"><br>
									<input type="text" name="e_phone_no" value="<?php echo $var_names['e_phone_no']; ?>"><br>
									<input type="text" name="e_commission" value="<?php echo $var_names['e_commission']; ?>"><br>
								</td>
							</tr>
						</table>
					</td>

					<!-- Vehicle table-->
					<td>
						<table>
							<tr>
								<th colspan = "4"> <h3> Vehicle Information </h3> </th>
							</tr>
							<tr>
								<td>
									<span class="error">*</span><b>Serial Number</b> <br>
									<span class="error">*</span>Make <br>
									<span class="error">*</span>Model <br>
									<span class="error">*</span>Year <br>
									<span class="error">*</span>Style <br>
								</td>
								<td>
									<input type="text" name="serial_no" value="<?php echo $var_names['serial_no']; ?>"><br>
									<input type="text" name="make" value="<?php echo $var_names['make']; ?>"><br>
									<input type="text" name="model" value="<?php echo $var_names['model']; ?>"><br>
									<input type="text" name="year" value="<?php echo $var_names['year']; ?>"><br>
									<input type="text" name="style" value="<?php echo $var_names['style']; ?>"><br>
								</td>
								<td>
									<span class="error">*</span>Miles <br>
									<span class="error">*</span>Exterior Colour <br>
									<span class="error">*</span>Interior Colour <br>
									<span class="error">*</span>Condition <br>
									<span class="error">*</span>Book Price <br>
								</td>
								<td>
									<input type="text" name="miles" value="<?php echo $var_names['miles']; ?>"><br>
									<input type="text" name="ext_colour" value="<?php echo $var_names['ext_colour']; ?>"><br>
									<input type="text" name="int_colour" value="<?php echo $var_names['int_colour']; ?>"><br>
									<input type="text" name="condition" value="<?php echo $var_names['condition']; ?>"><br>
									<input type="text" name="b_price" value="<?php echo $var_names['b_price']; ?>"><br>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<!-- CUSTOMER information and EMPLOYMENT HISTORY-->
				<tr>
					<!-- Customer table-->
					<td>
						<table>
							<tr>
								<th colspan = "2"> <h3> Customer </h3> </th>
							</tr>
							<tr>
								<td>
									<span class="error">*</span><b>SIN Number</b> <br>
								</td>
								<td>	
									<input type="text" name="c_sin" value="<?php echo $var_names['c_sin']; ?>"><br>
								</td>
							</tr>
							<tr>
								<td>
									<span class="error">*</span>Last Name <br>
								</td>
								<td>
									<input type="text" name="c_last_name" value="<?php echo $var_names['c_last_name']; ?>"><br>
								</td>
							</tr>
							<tr>
								<td>
									<span class="error">*</span>First Name <br>
								</td>
								<td>
									<input type="text" name="c_first_name" value="<?php echo $var_names['c_first_name']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>Phone Number <br>
								</td>
								<td>
									<input type="text" name="c_phone_no" value="<?php echo $var_names['c_phone_no']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>License Number <br>
								</td>
								<td>
									<input type="text" name="c_license_no" value="<?php echo $var_names['c_license_no']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>Address: <br> 
								</td>
								<td>
									
								</td>
							</tr>
							<tr>
								<td>
									<span class="error">*</span>Street Number <br>
								</td>
								<td>
									<input type="text" name="c_street_no" value="<?php echo $var_names['c_street_no']; ?>"><br>
								</td>
							</tr>
							<tr>
								<td>
									<span class="error">*</span>Street Name <br>
								</td>
								<td>
									<input type="text" name="c_street_name" value="<?php echo $var_names['c_street_name']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>City <br>
								</td>
								<td>
									<input type="text" name="c_city" value="<?php echo $var_names['c_city']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>Province <br>
								</td>
								<td>
									<input type="text" name="c_province" value="<?php echo $var_names['c_province']; ?>"><br>
								</td>
							</tr>		
							<tr>
								<td>
									<span class="error">*</span>Postal Code <br>
								</td>
								<td>
									<input type="text" name="c_postal_code" value="<?php echo $var_names['c_postal_code']; ?>"><br>
								</td>
							</tr>
						</table>
					</td>

					<!-- Employment History tale-->
					<td colspan = "2">
						<table>
							<tr>
								<th colspan = "6"> <h3> Employment History </h3> </th>
							</tr>
							<tr>
								<th> Employer </th>
								<th> Title </th>
								<th> Supervisor </th>
								<th> Phone Number </th>
								<th> Address </th>
								<th> Start Date </th>
							</tr>
							<tr>
								<td>
									<input type="text" name="h_employer_1" value="<?php echo $var_names['h_employer_1']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_title_1" value="<?php echo $var_names['h_title_1']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_supervisor_1" value="<?php echo $var_names['h_supervisor_1']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_phone_no_1" value="<?php echo $var_names['h_phone_no_1']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_address_1" value="<?php echo $var_names['h_address_1']; ?>"> |
								</td>
								<td>
									<input type="date" name="h_start_date_1" value="<?php echo $var_names['h_start_date_1']; ?>"> 
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="h_employer_2" value="<?php echo $var_names['h_employer_2']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_title_2" value="<?php echo $var_names['h_title_2']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_supervisor_2" value="<?php echo $var_names['h_supervisor_2']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_phone_no_2" value="<?php echo $var_names['h_phone_no_2']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_address_2" value="<?php echo $var_names['h_address_2']; ?>"> |
								</td>
								<td>
									<input type="date" name="h_start_date_2" value="<?php echo $var_names['h_start_date_2']; ?>"> 
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="h_employer_3" value="<?php echo $var_names['h_employer_3']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_title_3" value="<?php echo $var_names['h_title_3']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_supervisor_3" value="<?php echo $var_names['h_supervisor_3']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_phone_no_3" value="<?php echo $var_names['h_phone_no_3']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_address_3" value="<?php echo $var_names['h_address_3']; ?>"> |
								</td>
								<td>
									<input type="date" name="h_start_date_3" value="<?php echo $var_names['h_start_date_3']; ?>"> 
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="h_employer_4" value="<?php echo $var_names['h_employer_4']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_title_4" value="<?php echo $var_names['h_title_4']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_supervisor_4" value="<?php echo $var_names['h_supervisor_4']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_phone_no_4" value="<?php echo $var_names['h_phone_no_4']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_address_4" value="<?php echo $var_names['h_address_4']; ?>"> |
								</td>
								<td>
									<input type="date" name="h_start_date_4" value="<?php echo $var_names['h_start_date_4']; ?>"> 
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="h_employer_5" value="<?php echo $var_names['h_employer_5']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_title_5" value="<?php echo $var_names['h_title_5']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_supervisor_5" value="<?php echo $var_names['h_supervisor_5']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_phone_no_5" value="<?php echo $var_names['h_phone_no_5']; ?>"> |
								</td>
								<td>
									<input type="text" name="h_address_5" value="<?php echo $var_names['h_address_5']; ?>"> |
								</td>
								<td>
									<input type="date" name="h_start_date_5" value="<?php echo $var_names['h_start_date_5']; ?>"> 
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
 
			<br> <input type="submit" name="search" value="Search"> <input type="submit" name="submit" value="Submit"> <br>
			<i> Search for bolded values input. </i><br>
			</form>

			<!-- QUERY SECTION -->
			<!---------------------------------------------------------------------------------------------------
				This section will hold all information related to the queries, including a check for all REQUIRED 
				variables before sending data to the database. 
			---------------------------------------------------------------------------------------------------->
			<?php
				if ($can_send == TRUE){
				
					// Check to make sure vehicle has not already been purchased:
					$sql_get_sale_id = "SELECT sale_no FROM vehicle_sale_history WHERE serial_no = " . $var_names['serial_no'];

					$result = $conn -> query($sql_get_sale_id);

					if ($result != FALSE && $result -> num_rows > 0){
						die("<br><span class='error'>Error in submitting: This vehicle has already been sold.</span><br>");
					}

					// Create query to insert into customer
					$result = $conn -> query($sql_get_customer_info);
					$customer_id = "";

					if ($result -> num_rows <= 0) {
						$sql_ins_customer = "INSERT INTO Customers (license_no, customer_SIN, first_name, last_name,
						phone_no, addr_street_no, addr_street, addr_city, addr_province, addr_postal_code) ";

						$sql_ins_customer .= "VALUES ('". $var_names['c_license_no'] ."', '". $var_names['c_sin'] ."', '". $var_names['c_first_name'] ."',
						'". $var_names['c_last_name'] ."', '". $var_names['c_phone_no'] ."', '". $var_names['c_street_no'] ."',
						'". $var_names['c_street_name'] ."', '". $var_names['c_city'] ."', '". $var_names['c_province'] ."', '". $var_names['c_postal_code'] ."')";
				
						$result = $conn -> query($sql_ins_customer);

						if ($result != FALSE)
						{
							$customer_id = $conn -> insert_id;
						}
					} else {
						$row = $result -> fetch_assoc();
						$customer_id = $row['customer_id'];
					}

					// Create query to insert into employment
					$employment_hist_id_arr = array();

					if ($employment_hist_count_arr['h_1'] >= 1) {
						$sql_get_employment = "SELECT * FROM Employment WHERE start_date = '" . $var_names['h_start_date_1'] . 
						"' AND employer = '" . $var_names['h_employer_1'] . "' AND title = '" . $var_names['h_title_1'] .
						"' AND supervisor = '" . $var_names['h_supervisor_1'] . "' AND phone_no = '" . $var_names['h_phone_no_1'] .
						"' AND addr = '" . $var_names['h_address_1'] ."'";

						$result = $conn -> query($sql_get_employment);

						if ($result != FALSE && $result -> num_rows > 0){
							$row = $result -> fetch_assoc();
							$emp_id = $row['employment_id'];
							array_push($employment_hist_id_arr, $emp_id);
						} else {
							$sql_ins_employment = "INSERT INTO EMPLOYMENT (start_date, employer, title, supervisor, phone_no, addr) ";
							$sql_ins_employment .= "VALUES ('". $var_names['h_start_date_1'] ."', '". $var_names['h_employer_1'] ."',
							'". $var_names['h_title_1'] ."', '". $var_names['h_supervisor_1'] ."', '". $var_names['h_phone_no_1'] ."', '". $var_names['h_address_1'] ."')";

							$result = $conn -> query($sql_ins_employment);

							if ($result != FALSE){
								$emp_id = $conn -> insert_id;
								array_push($employment_hist_id_arr, $emp_id);
							}
						}
					}

					if ($employment_hist_count_arr['h_2'] >= 1) {
						$sql_get_employment = "SELECT * FROM Employment WHERE start_date = '" . $var_names['h_start_date_2'] . 
						"' AND employer = '" . $var_names['h_employer_2'] . "' AND title = '" . $var_names['h_title_2'] .
						"' AND supervisor = '" . $var_names['h_supervisor_2'] . "' AND phone_no = '" . $var_names['h_phone_no_2'] .
						"' AND addr = '" . $var_names['h_address_2'] . "'";

						$result = $conn -> query($sql_get_employment);

						if ($result != FALSE && $result -> num_rows > 0){
							$row = $result -> fetch_assoc();
							$emp_id = $row['employment_id'];
							array_push($employment_hist_id_arr, $emp_id);
						} else {
							$sql_ins_employment = "INSERT INTO EMPLOYMENT (start_date, employer, title, supervisor, phone_no, addr) ";
							$sql_ins_employment .= "VALUES ('". $var_names['h_start_date_2'] ."', '". $var_names['h_employer_2'] ."',
							'". $var_names['h_title_2'] ."', '". $var_names['h_supervisor_2'] ."', '". $var_names['h_phone_no_2'] ."', '". $var_names['h_address_2'] ."')";

							$result = $conn -> query($sql_ins_employment);

							if ($result != FALSE){
								$emp_id = $conn -> insert_id;
								array_push($employment_hist_id_arr, $emp_id);
							}
						}
					}

					if ($employment_hist_count_arr['h_3'] >= 1) {
						$sql_get_employment = "SELECT * FROM Employment WHERE start_date = '" . $var_names['h_start_date_3'] . 
						"' AND employer = '" . $var_names['h_employer_3'] . "' AND title = '" . $var_names['h_title_3'] .
						"' AND supervisor = '" . $var_names['h_supervisor_3'] . "' AND phone_no = '" . $var_names['h_phone_no_3'] .
						"' AND addr = '" . $var_names['h_address_3'] . "'";

						$result = $conn -> query($sql_get_employment);

						if ($result != FALSE && $result -> num_rows > 0){
							$row = $result -> fetch_assoc();
							$emp_id = $row['employment_id'];
							array_push($employment_hist_id_arr, $emp_id);
						} else {
							$sql_ins_employment = "INSERT INTO EMPLOYMENT (start_date, employer, title, supervisor, phone_no, addr) ";
							$sql_ins_employment .= "VALUES ('". $var_names['h_start_date_3'] ."', '". $var_names['h_employer_3'] ."',
							'". $var_names['h_title_3'] ."', '". $var_names['h_supervisor_3'] ."', '". $var_names['h_phone_no_3'] ."', '". $var_names['h_address_3'] ."')";

							$result = $conn -> query($sql_ins_employment);

							if ($result != FALSE){
								$emp_id = $conn -> insert_id;
								array_push($employment_hist_id_arr, $emp_id);
							}
						}
					}

					if ($employment_hist_count_arr['h_4'] >= 1) {
						$sql_get_employment = "SELECT * FROM Employment WHERE start_date = '" . $var_names['h_start_date_4'] . 
						"' AND employer = '" . $var_names['h_employer_4'] . "' AND title = '" . $var_names['h_title_4'] .
						"' AND supervisor = '" . $var_names['h_supervisor_4'] . "' AND phone_no = '" . $var_names['h_phone_no_4'] .
						"' AND addr = '" . $var_names['h_address_4'] . "'";

						$result = $conn -> query($sql_get_employment);

						if ($result != FALSE && $result -> num_rows > 0){
							$row = $result -> fetch_assoc();
							$emp_id = $row['employment_id'];
							array_push($employment_hist_id_arr, $emp_id);
						} else {
							$sql_ins_employment = "INSERT INTO EMPLOYMENT (start_date, employer, title, supervisor, phone_no, addr) ";
							$sql_ins_employment .= "VALUES ('". $var_names['h_start_date_4'] ."', '". $var_names['h_employer_4'] ."',
							'". $var_names['h_title_4'] ."', '". $var_names['h_supervisor_4'] ."', '". $var_names['h_phone_no_4'] ."', '". $var_names['h_address_4'] ."')";

							$result = $conn -> query($sql_ins_employment);

							if ($result != FALSE){
								$emp_id = $conn -> insert_id;
								array_push($employment_hist_id_arr, $emp_id);
							}
						}
					}

					if ($employment_hist_count_arr['h_5'] >= 1) {
						$sql_get_employment = "SELECT * FROM Employment WHERE start_date = '" . $var_names['h_start_date_5'] . 
						"' AND employer = '" . $var_names['h_employer_5'] . "' AND title = '" . $var_names['h_title_5'] .
						"' AND supervisor = '" . $var_names['h_supervisor_5'] . "' AND phone_no = '" . $var_names['h_phone_no_5'] .
						"' AND addr = '" . $var_names['h_address_5'] . "'";

						$result = $conn -> query($sql_get_employment);

						if ($result != FALSE && $result -> num_rows > 0){
							$row = $result -> fetch_assoc();
							$emp_id = $row['employment_id'];
							array_push($employment_hist_id_arr, $emp_id);
						} else {
							$sql_ins_employment = "INSERT INTO EMPLOYMENT (start_date, employer, title, supervisor, phone_no, addr) ";
							$sql_ins_employment .= "VALUES ('". $var_names['h_start_date_5'] ."', '". $var_names['h_employer_5'] ."',
							'". $var_names['h_title_5'] ."', '". $var_names['h_supervisor_5'] ."', '". $var_names['h_phone_no_5'] ."', '". $var_names['h_address_5'] ."')";

							$result = $conn -> query($sql_ins_employment);

							if ($result != FALSE){
								$emp_id = $conn -> insert_id;
								array_push($employment_hist_id_arr, $emp_id);
							}
						}
					}

					// Create query to insert into employment_history
					foreach ($employment_hist_id_arr as $emp_id){
						$sql_emp_hist = "INSERT INTO employment_history (customer_id, employment_id) ";
						$sql_emp_hist .= "VALUES ('". $customer_id ."', '". $emp_id ."')";

						$conn -> query($sql_emp_hist);
					}


					// Create query to insert into sale
					$sql_ins_sale = "INSERT INTO sales (sale_date, total_due, down_payment, finance_amount) ";
					$sql_ins_sale .= "VALUES ('". $var_names['date'] ."', '". $var_names['total_due'] ."', '". $var_names['down_payment'] ."', '". $var_names['finance_amount'] ."')";

					$conn -> query($sql_ins_sale);

					$sale_id = $conn -> insert_id;

					// Create query to insert into vehicle_sale_history
					$sql_ins_vehicle_sale = "INSERT INTO vehicle_sale_history (serial_no, sale_no) ";
					$sql_ins_vehicle_sale .= "VALUES ('". $var_names['serial_no'] ."', '". $sale_id ."')";

					$conn -> query($sql_ins_vehicle_sale);

					// Create query to insert into customer_sale_history
					$sql_ins_customer_sale = "INSERT INTO customer_sale_history (customer_id, sale_no) ";
					$sql_ins_customer_sale .= "VALUES ('". $customer_id ."', '". $sale_id ."')";

					$conn -> query($sql_ins_customer_sale);

					// Create query to insert into employee_sale_history
					$sql_ins_emp_sale = "INSERT INTO employee_sale_history (SIN, sale_no, commission) ";
					$sql_ins_emp_sale .= "VALUES ('". $var_names['e_sin'] ."', '". $sale_id ."', '". $var_names['e_commission'] ."')";
					
					$conn -> query($sql_ins_emp_sale);

					echo "<br><h3>Purchase processed: Thank you.</h3><br>";
					$repeating_send = TRUE;

				} elseif ($_SERVER["REQUEST_METHOD"] == "POST"){
					
					echo "<br><span class='error'>Please populate all required fields before submitting.</span><br>";

				}	

				
				$conn -> close();
					
			?>

        </div>
	</div>
</body>
</html>