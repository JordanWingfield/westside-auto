<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="Styles/stylesheet.css">
	<title>West Side Auto</title>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
		<div class="main_grid">
        <style>
        table, td {
            border: 1px solid black;
        }
        th {
            border: 1px solid black;
            font-size: large;
        }
        </style>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <fieldset>
            <legend>Customer Look-Up</legend>
            <select name="qtype">
                <option value="Select One" disabled selected>Select One</option>
                <option value="All">All</option>
                <option value="Name">Name</option>
                <option value="City">City</option>
                <option value="Phone Number">Phone Number</option>
                <option value="SIN">SIN</option>
            </select>
            <input type="text" name="query">
            <input type="submit" value="Submit">
            </fieldset>
        </form>
        <?php
            mysql_connect("localhost", "root", "") or die("Error connecting to database: ".mysql_error());
     
            mysql_select_db("westside_auto") or die(mysql_error());
        ?>
        <?php    
        if((isset($_POST['query'])) && (isset($_POST['qtype']))){  
            $query = $_POST['query'];
            $qtype = $_POST['qtype'];
        
        $min_length = 3;
        // you can set minimum length of the query if you want
     
        if((strlen($query) >= $min_length) || ($qtype==='All')){ // if query length is more or equal minimum length or all is selected
         
            $query = htmlspecialchars($query); 
            // changes characters used in html to their equivalents, for example: < to &gt;
            $query = mysql_real_escape_string($query);
            // makes sure nobody uses SQL injection
        
        if($qtype==='All'){
            $raw_results = mysql_query("SELECT * FROM customers"); 
            echo "<br>";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Customer ID</th>";
                    echo "<th>License Plate</th>";
                    echo "<th>SIN</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Phone Number</th>";
                    echo "<th>Address</th>";
                    echo "<th>City</th>";
                    echo "<th>Province</th>";
                    echo "<th>Postal Code</th>";
                echo "</tr>";
            while($results = mysql_fetch_array($raw_results)){
                // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                echo "<tr>";
                    echo "<td>".$results['customer_id']."</td>";
                    echo "<td>".$results['license_no']."</td>";
                    echo "<td>".$results['customer_SIN']."</td>";
                    echo "<td>".$results['first_name']."</td>";
                    echo "<td>".$results['last_name']."</td>";
                    echo "<td>".$results['phone_no']."</td>";
                    echo "<td>".$results['addr_street_no']." ".$results['addr_street']."</td>";
                    echo "<td>".$results['addr_city']."</td>";
                    echo "<td>".$results['addr_province']."</td>";
                    echo "<td>".$results['addr_postal_code']."</td>";
                    // posts results gotten from database into a table
                echo "</tr>";
                }
                echo "</table>";
            }
        elseif($qtype==='Name'){

        $raw_results = mysql_query("SELECT * FROM customers
            WHERE (`first_name` LIKE '%".$query."%') OR (`last_name` LIKE '%".$query."%')") or die(mysql_error());
        
        // '%$query%' is what we're looking for, % means anything, for example if $query is Hello
        // it will match "hello", "Hello man", "gogohello", if you want exact match use `title`='$query'
        // or if you want to match just full word so "gogohello" is out use '% $query %' ...OR ... '$query %' ... OR ... '% $query'
         
        if(mysql_num_rows($raw_results) > 0){ // if one or more rows are returned do following
            echo "<br>";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Customer ID</th>";
                    echo "<th>License Plate</th>";
                    echo "<th>SIN</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Phone Number</th>";
                    echo "<th>Address</th>";
                    echo "<th>City</th>";
                    echo "<th>Province</th>";
                    echo "<th>Postal Code</th>";
                echo "</tr>";
            while($results = mysql_fetch_array($raw_results)){
                echo "<tr>";
                    echo "<td>".$results['customer_id']."</td>";
                    echo "<td>".$results['license_no']."</td>";
                    echo "<td>".$results['customer_SIN']."</td>";
                    echo "<td>".$results['first_name']."</td>";
                    echo "<td>".$results['last_name']."</td>";
                    echo "<td>".$results['phone_no']."</td>";
                    echo "<td>".$results['addr_street_no']." ".$results['addr_street']."</td>";
                    echo "<td>".$results['addr_city']."</td>";
                    echo "<td>".$results['addr_province']."</td>";
                    echo "<td>".$results['addr_postal_code']."</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
        else{ // if there is no matching rows do following
            echo "No results";
        }
    }
        elseif($qtype==='City'){

        $raw_results = mysql_query("SELECT * FROM customers
            WHERE (`addr_city`='$query')") or die(mysql_error());
            
        if(mysql_num_rows($raw_results) > 0){
            echo "<br>";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Customer ID</th>";
                    echo "<th>License Plate</th>";
                    echo "<th>SIN</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Phone Number</th>";
                    echo "<th>Address</th>";
                    echo "<th>City</th>";
                    echo "<th>Province</th>";
                    echo "<th>Postal Code</th>";
                echo "</tr>";
            while($results = mysql_fetch_array($raw_results)){
                echo "<tr>";
                    echo "<td>".$results['customer_id']."</td>";
                    echo "<td>".$results['license_no']."</td>";
                    echo "<td>".$results['customer_SIN']."</td>";
                    echo "<td>".$results['first_name']."</td>";
                    echo "<td>".$results['last_name']."</td>";
                    echo "<td>".$results['phone_no']."</td>";
                    echo "<td>".$results['addr_street_no']." ".$results['addr_street']."</td>";
                    echo "<td>".$results['addr_city']."</td>";
                    echo "<td>".$results['addr_province']."</td>";
                    echo "<td>".$results['addr_postal_code']."</td>";
                echo "</tr>";
                }
            echo "</table>";
            }
            else{ 
                echo "No results";
            }
    }
    elseif($qtype==='Phone Number'){
        function formatPhone($query){
            $query = preg_replace('/[^\d]/', '', $query); //Remove anything that is not a number
            if(strlen($query) != 10){
                    echo "<br>";
                    echo "Please enter 10 digits for phone number\n";
            }
            return substr($query, 0, 3) . '-' . substr($query, 3, 3) . '-' . substr($query, 6);
        }
        $query = formatPhone($query);
        $raw_results = mysql_query("SELECT * FROM customers
            WHERE (`phone_no`='$query')") or die(mysql_error());
            
        if(mysql_num_rows($raw_results) > 0){
            echo "<br>";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Customer ID</th>";
                    echo "<th>License Plate</th>";
                    echo "<th>SIN</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Phone Number</th>";
                    echo "<th>Address</th>";
                    echo "<th>City</th>";
                    echo "<th>Province</th>";
                    echo "<th>Postal Code</th>";
                echo "</tr>";
                while($results = mysql_fetch_array($raw_results)){
                echo "<tr>";
                    echo "<td>".$results['customer_id']."</td>";
                    echo "<td>".$results['license_no']."</td>";
                    echo "<td>".$results['customer_SIN']."</td>";
                    echo "<td>".$results['first_name']."</td>";
                    echo "<td>".$results['last_name']."</td>";
                    echo "<td>".$results['phone_no']."</td>";
                    echo "<td>".$results['addr_street_no']." ".$results['addr_street']."</td>";
                    echo "<td>".$results['addr_city']."</td>";
                    echo "<td>".$results['addr_province']."</td>";
                    echo "<td>".$results['addr_postal_code']."</td>";
                echo "</tr>";
            }
            echo "</table>";
            }
            else{ 
                echo "No results";
            }
    }
    elseif($qtype==='SIN'){
        function formatSIN($query){
            $query = preg_replace('/[^\d]/', '', $query); 
            if(strlen($query) != 9){
                echo "<br>";    
                echo "Please enter 9 digits for SIN\n";
            }
            return $query;
        }
        $query = formatSIN($query);
        $raw_results = mysql_query("SELECT * FROM customers
            WHERE (`customer_SIN`='$query')") or die(mysql_error());
            
        if(mysql_num_rows($raw_results) > 0){
            echo "<br>";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Customer ID</th>";
                    echo "<th>License Plate</th>";
                    echo "<th>SIN</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Phone Number</th>";
                    echo "<th>Address</th>";
                    echo "<th>City</th>";
                    echo "<th>Province</th>";
                    echo "<th>Postal Code</th>";
                echo "</tr>";
                while($results = mysql_fetch_array($raw_results)){
                echo "<tr>";
                    echo "<td>".$results['customer_id']."</td>";
                    echo "<td>".$results['license_no']."</td>";
                    echo "<td>".$results['customer_SIN']."</td>";
                    echo "<td>".$results['first_name']."</td>";
                    echo "<td>".$results['last_name']."</td>";
                    echo "<td>".$results['phone_no']."</td>";
                    echo "<td>".$results['addr_street_no']." ".$results['addr_street']."</td>";
                    echo "<td>".$results['addr_city']."</td>";
                    echo "<td>".$results['addr_province']."</td>";
                    echo "<td>".$results['addr_postal_code']."</td>";
                echo "</tr>";
            }
            echo "</table>";
            }
            else{ 
                echo "No results";
            }
    } 
    }
    else{ // if query length is less than minimum
        echo "Minimum length is ".$min_length;
    }
}
?>
		</div>
	</div>
</body>
</html>