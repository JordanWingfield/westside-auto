<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="Styles/stylesheet.css">
	<title>West Side Auto</title>
</head>
<body>
	<div class="wrapper">
		<div class="title_bar">
			West Side Auto
		</div>
		<div class="side_bar">
			<a href="index.php">Home</a><br/>
			<a href="report_stock_search.php">View Stock</a><br/>
			<a href="report_customer_look-up.php">Search Customers</a><br/>
			<a href="form_buy_car.php">Purchase Vehicle</a><br/>
			<a href="form_sell_car.php">Sell Vehicle</a><br/>
			<a href="report_employee_sale_history.php">Sale History</a><br/>
			<a href="form&report_repair_report.php">Repair History</a><br/>
		</div>
		<div class="main_grid">
		<style>
        table, td {
            border: 1px solid black;
        }
        th {
            border: 1px solid black;
            font-size: large;
        }
        </style>
		<?php
					
					// Connect to database:
					$servername = "localhost";
					$username = "root";
					$password = "";
					$dbname = "westside_auto";

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn -> connect_error){
						die("Unable to connect to database at this time: Please try again later, or contact support.");
					}
					$make = "SELECT distinct make FROM vehicles";
					$make_result = $conn -> query($make);
					$condition = $model = $carmake = $style = "";
		?>			
					<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					<fieldset>
					<legend>Stock Search</legend>
					Make:
					<?php
					echo "<select name='carmake' style='width: 80px;'>";
					echo "<option value=''>".' '."</option>";
					echo "<option value='All'>".All."</option>";
					while ($row = mysqli_fetch_array($make_result)) {
    					echo "<option value='" . $row['make'] . "'>" . $row['make'] . "</option>";
					}
					echo "</select>";
					?>
					Model:
					<input type="text" name="model" style="width: 120px">
					Condition:
					<input type="radio" name="condition" <?php if (isset($condition) && $condition=="New") echo "checked";?> value="New">New
					<input type="radio" name="condition" <?php if (isset($condition) && $condition=="Used") echo "checked";?> value="Used">Used
					<br>
					<br>
					Style:
					<select name="style" style="width: 80px">
						<option value=" "></option>
						<option value="sedan">Sedan</option>
						<option value="coupe">Coupe</option>
						<option value="hatch">Hatch</option>
						<option value="truck">Truck</option>
						<option value="van">Van</option>
						<option value="pick-up">Pick-up</option>
					</select>
					Year:
					<select name="minyear">
					<?php 
					echo "<option>".' '."</option>";
   					for($minyear = 1999; $minyear <= date('Y'); $minyear++){
						echo "<option>$minyear</option>";
					   }
					?>
					</select>
					to
					<select name="maxyear">
					<?php 
					echo "<option>".' '."</option>";
   					for($maxyear = date('Y'); $maxyear > 1999; $maxyear--){
						echo "<option>$maxyear</option>";
					   }
					?>
					</select>
					<br>
					<br>
					Miles:
					<input type="text" name="minmiles" style="width: 100px">
					to
					<input type="text" name="maxmiles" style="width: 100px">
					<br>
					<br>
					Price:
					<input type="text" name="minprice" style="width: 100px">
					to
					<input type="text" name="maxprice" style="width: 100px">
					<br>
					<br>
					<input type="submit" value="Submit">
					</fieldset>
					</form>
					<?php
					if($_SERVER["REQUEST_METHOD"] == "POST"){
						if(isset($_POST['carmake'])){
							$carmake = $_POST['carmake'];
						} else {
							$carmake = "";
						}
						if(isset($_POST['model'])){
							$model = $_POST['model'];
						} else {
							$model = "";
						}
						if(isset($_POST['style'])){
							$style = $_POST['style'];
						} else {
							$style = "";
						}
						if(isset($_POST['condition'])){
							$condition = $_POST['condition'];
						} else {
							$condition = "";
						}
						if(isset($_POST['minyear'])){
							$minyear = $_POST['minyear'];
						} else {
							$minyear = "";
						}
						if(isset($_POST['maxyear'])){
							$maxyear = $_POST['maxyear'];
						} else {
							$maxyear = "";
						}
						if(isset($_POST['minmiles'])){
							$minmiles = $_POST['minmiles'];
						} else {
							$minmiles = "";
						}
						if(isset($_POST['maxmiles'])){
							$maxmiles = $_POST['maxmiles'];
						} else {
							$maxmiles = "";
						}
						if(isset($_POST['minprice'])){
							$minprice = $_POST['minprice'];
						} else {
							$minprice = "";
						}
						if(isset($_POST['maxprice'])){
							$maxprice = $_POST['maxprice'];
						} else {
							$maxprice = "";
						}
					    
						if($carmake==='All'){
						$raw_results = $conn -> query("SELECT * FROM vehicles"); 
            			echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
                    				echo "<th>Style</th>";
                    				echo "<th>Miles</th>";
                    				echo "<th>Exterior Colour</th>";
                    				echo "<th>Interior Colour</th>";
                    				echo "<th>Condition</th>";
                    				echo "<th>Price</th>";
                				echo "</tr>";
            				while($results = mysqli_fetch_array($raw_results)){
                				// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                				echo "<tr>";
                    				echo "<td>".$results['serial_no']."</td>";
                    				echo "<td>".$results['make']."</td>";
                    				echo "<td>".$results['model']."</td>";
                    				echo "<td>".$results['year']."</td>";
                    				echo "<td>".$results['style']."</td>";
                    				echo "<td>".$results['miles']."</td>";
                    				echo "<td>".$results['ext_colour']."</td>";
                    				echo "<td>".$results['int_colour']."</td>";
                    				echo "<td>".$results['vehicle_condition']."</td>";
                    				echo "<td>".$results['book_price']."</td>";
                    				// posts results gotten from database into a table
                				echo "</tr>";
                				}
                			echo "</table>";
						}
						if((!empty($carmake)) && ($carmake!='All')){
							$raw_results = $conn -> query("SELECT * FROM vehicles
								WHERE (`make`='$carmake')") or die(mysql_error());
            			echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
                    				echo "<th>Style</th>";
                    				echo "<th>Miles</th>";
                    				echo "<th>Exterior Colour</th>";
                    				echo "<th>Interior Colour</th>";
                    				echo "<th>Condition</th>";
                    				echo "<th>Price</th>";
                				echo "</tr>";
            				while($results = mysqli_fetch_array($raw_results)){
                				// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                				echo "<tr>";
                    				echo "<td>".$results['serial_no']."</td>";
                    				echo "<td>".$results['make']."</td>";
                    				echo "<td>".$results['model']."</td>";
                    				echo "<td>".$results['year']."</td>";
                    				echo "<td>".$results['style']."</td>";
                    				echo "<td>".$results['miles']."</td>";
                    				echo "<td>".$results['ext_colour']."</td>";
                    				echo "<td>".$results['int_colour']."</td>";
                    				echo "<td>".$results['vehicle_condition']."</td>";
                    				echo "<td>".$results['book_price']."</td>";
                    				// posts results gotten from database into a table
                				echo "</tr>";
                				}
                			echo "</table>";
						}
						if(!empty($model)){
							$raw_results = $conn -> query("SELECT * FROM vehicles
								WHERE (`model`='$model')") or die(mysql_error());
								if(mysqli_num_rows($raw_results) > 0){
            			echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
                    				echo "<th>Style</th>";
                    				echo "<th>Miles</th>";
                    				echo "<th>Exterior Colour</th>";
                    				echo "<th>Interior Colour</th>";
                    				echo "<th>Condition</th>";
                    				echo "<th>Price</th>";
                				echo "</tr>";
            				while($results = mysqli_fetch_array($raw_results)){
                				// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                				echo "<tr>";
                    				echo "<td>".$results['serial_no']."</td>";
                    				echo "<td>".$results['make']."</td>";
                    				echo "<td>".$results['model']."</td>";
                    				echo "<td>".$results['year']."</td>";
                    				echo "<td>".$results['style']."</td>";
                    				echo "<td>".$results['miles']."</td>";
                    				echo "<td>".$results['ext_colour']."</td>";
                    				echo "<td>".$results['int_colour']."</td>";
                    				echo "<td>".$results['vehicle_condition']."</td>";
                    				echo "<td>".$results['book_price']."</td>";
                    				// posts results gotten from database into a table
                				echo "</tr>";
                				}
                			echo "</table>";
						} else {
							echo "<br>";
							echo "<h3>No Records Found, Please Try Again</h3>";
						}
						}
						if($condition==='New'){
							$raw_results = $conn -> query("SELECT * FROM vehicles
								WHERE (`vehicle_condition`='$condition')") or die(mysql_error()); 
            			echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
                    				echo "<th>Style</th>";
                    				echo "<th>Miles</th>";
                    				echo "<th>Exterior Colour</th>";
                    				echo "<th>Interior Colour</th>";
                    				echo "<th>Condition</th>";
                    				echo "<th>Price</th>";
                				echo "</tr>";
            				while($results = mysqli_fetch_array($raw_results)){
                				// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                				echo "<tr>";
                    				echo "<td>".$results['serial_no']."</td>";
                    				echo "<td>".$results['make']."</td>";
                    				echo "<td>".$results['model']."</td>";
                    				echo "<td>".$results['year']."</td>";
                    				echo "<td>".$results['style']."</td>";
                    				echo "<td>".$results['miles']."</td>";
                    				echo "<td>".$results['ext_colour']."</td>";
                    				echo "<td>".$results['int_colour']."</td>";
                    				echo "<td>".$results['vehicle_condition']."</td>";
                    				echo "<td>".$results['book_price']."</td>";
                    				// posts results gotten from database into a table
                				echo "</tr>";
                				}
                			echo "</table>";
						} elseif($condition==='Used'){
							$raw_results = $conn -> query("SELECT * FROM vehicles
								WHERE (`vehicle_condition`='$condition')") or die(mysql_error()); 
            			echo "<br>";
            				echo "<table>";
                				echo "<tr>";
                    				echo "<th>Serial No.</th>";
                    				echo "<th>Make</th>";
                    				echo "<th>Model</th>";
                    				echo "<th>Year</th>";
                    				echo "<th>Style</th>";
                    				echo "<th>Miles</th>";
                    				echo "<th>Exterior Colour</th>";
                    				echo "<th>Interior Colour</th>";
                    				echo "<th>Condition</th>";
                    				echo "<th>Price</th>";
                				echo "</tr>";
            				while($results = mysqli_fetch_array($raw_results)){
                				// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                				echo "<tr>";
                    				echo "<td>".$results['serial_no']."</td>";
                    				echo "<td>".$results['make']."</td>";
                    				echo "<td>".$results['model']."</td>";
                    				echo "<td>".$results['year']."</td>";
                    				echo "<td>".$results['style']."</td>";
                    				echo "<td>".$results['miles']."</td>";
                    				echo "<td>".$results['ext_colour']."</td>";
                    				echo "<td>".$results['int_colour']."</td>";
                    				echo "<td>".$results['vehicle_condition']."</td>";
                    				echo "<td>".$results['book_price']."</td>";
                    				// posts results gotten from database into a table
                				echo "</tr>";
                				}
                			echo "</table>";
					}
					if((isset($style)) && ($style!=" ")){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`style`='$style')") or die(mysql_error()); 
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
					}
					if((isset($minyear)) && (!empty($minyear)) && (isset($maxyear)) && (!empty($maxyear))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`year` BETWEEN '$minyear' AND '$maxyear')") or die(mysql_error());
							if(mysqli_num_rows($raw_results) > 0){ 
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					} elseif((isset($minyear)) && (!empty($minyear))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`year` >= '$minyear')") or die(mysql_error()); 
							if(mysqli_num_rows($raw_results) > 0){
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					}
					if((isset($minmiles)) && (!empty($minmiles)) && (isset($maxmiles)) && (!empty($maxmiles))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`miles` BETWEEN '$minmiles' AND '$maxmiles')") or die(mysql_error());
							if(mysqli_num_rows($raw_results) > 0){ 
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					} elseif((isset($minmiles)) && (!empty($minmiles))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`miles` >= '$minmiles')") or die(mysql_error());
							if(mysqli_num_rows($raw_results) > 0){ 
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					}
					if((isset($minprice)) && (!empty($minprice)) && (isset($maxprice)) && (!empty($maxprice))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`book_price` BETWEEN '$minprice' AND '$maxprice')") or die(mysql_error());
							if(mysqli_num_rows($raw_results) > 0){
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					} elseif((isset($minprice)) && (!empty($minprice))){
						$raw_results = $conn -> query("SELECT * FROM vehicles
							WHERE (`book_price` >= '$minprice')") or die(mysql_error());
							if(mysqli_num_rows($raw_results) > 0){ 
					echo "<br>";
						echo "<table>";
							echo "<tr>";
								echo "<th>Serial No.</th>";
								echo "<th>Make</th>";
								echo "<th>Model</th>";
								echo "<th>Year</th>";
								echo "<th>Style</th>";
								echo "<th>Miles</th>";
								echo "<th>Exterior Colour</th>";
								echo "<th>Interior Colour</th>";
								echo "<th>Condition</th>";
								echo "<th>Price</th>";
							echo "</tr>";
						while($results = mysqli_fetch_array($raw_results)){
							// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
							echo "<tr>";
								echo "<td>".$results['serial_no']."</td>";
								echo "<td>".$results['make']."</td>";
								echo "<td>".$results['model']."</td>";
								echo "<td>".$results['year']."</td>";
								echo "<td>".$results['style']."</td>";
								echo "<td>".$results['miles']."</td>";
								echo "<td>".$results['ext_colour']."</td>";
								echo "<td>".$results['int_colour']."</td>";
								echo "<td>".$results['vehicle_condition']."</td>";
								echo "<td>".$results['book_price']."</td>";
								// posts results gotten from database into a table
							echo "</tr>";
							}
						echo "</table>";
							} else {
								echo "<br>";
								echo "<h3>No Records Found, Please Try Again</h3>";
							}
					}
				}
					?>
		</div>
	</div>
</body>
</html>