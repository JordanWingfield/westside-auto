﻿<?php
//-----------------------------------------------------
// SQL Query library
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

// Function INSERT_INTO_TABLE:
// Input: string representation of SQL query
// Output: success / error via echo
// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
function insert_into_table($conn, $sql, $tbname)
{
	if ($conn->query($sql) === TRUE) {
		echo "Entity created successfully in " . $tbname . ".";
	} else {
		echo "Error creating entity in " . $tbname . ": " . $conn->error;
	}
	echo "<br>";
}

?>
