﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "VEHICLE_SALE_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$v_result = $conn->query("SELECT serial_no FROM Vehicles");
$s_result = $conn->query("SELECT sale_no FROM Sales");

$sql_vehicles_arr = array();
$sql_sales_arr = array();

if ($v_result -> num_rows > 0){
	while($row = $v_result -> fetch_assoc()){
		array_push($sql_vehicles_arr, $row["serial_no"]);
	}
} else {
	echo "0 Entries found in VEHICLES";
}

if ($s_result -> num_rows > 0){
	while($row = $s_result -> fetch_assoc()){
		array_push($sql_sales_arr, $row["sale_no"]);
	}
} else {
	echo "0 Entries found in SALES";
}

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO vehicle_sale_history (serial_no, sale_no) ";

	$values = "VALUES ('". $sql_vehicles_arr[$i] ."', '".$sql_sales_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
