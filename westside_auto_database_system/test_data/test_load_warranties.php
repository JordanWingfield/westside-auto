﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "WARRANTIES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:

$warranty_name_arr = array("Minimal", "Moderate", "Full", "Minimal", "Moderate", "Full", "Minimal", "Moderate", "Full", "Minimal");

$start_date_arr = array("2016-12-17","2017-03-25","2015-09-03","2016-06-05","2015-10-12","2016-04-19","2015-04-22","2017-05-20","2016-08-23","2017-03-27");

$deductable_arr = array(1000,500,0,1000,500,0,1000,500,0,1000);

$cost_arr = array(1000,5000,10000,1000,5000,10000,1000,5000,10000,1000);

$coverage_description_arr = array("SOL", "funcitonal but could be better", "We gotchu fam", "SOL", "funcitonal but could be better", "We gotchu fam", "SOL", "funcitonal but could be better", "We gotchu fam", "SOL");
// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO warranties (warranty_name, start_date, deductable, cost, coverage_description) ";

	$values = "VALUES ('". $warranty_name_arr[$i] ."', '".$start_date_arr[$i]."',
	'". $deductable_arr[$i] ."', '".$cost_arr[$i]."', '".$coverage_description_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
