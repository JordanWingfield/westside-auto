﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "CUSTOMER_SALE_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$c_result = $conn->query("SELECT customer_id FROM Customers");
$s_result = $conn->query("SELECT sale_no FROM Sales");

$sql_customers_arr = array();
$sql_sales_arr = array();

if ($c_result -> num_rows > 0){
	while($row = $c_result -> fetch_assoc()){
		array_push($sql_customers_arr, $row["customer_id"]);
	}
} else {
	echo "0 Entries found in CUSTOMERS";
}

if ($s_result -> num_rows > 0){
	while($row = $s_result -> fetch_assoc()){
		array_push($sql_sales_arr, $row["sale_no"]);
	}
} else {
	echo "0 Entries found in SALES";
}

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO customer_sale_history (customer_id, sale_no) ";

	$values = "VALUES ('". $sql_customers_arr[$i] ."', '".$sql_sales_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
