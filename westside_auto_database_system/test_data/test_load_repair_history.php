﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "REPAIR_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$r_result = $conn->query("SELECT repair_id FROM Repairs");
$v_result = $conn->query("SELECT serial_no FROM Vehicles");

$sql_repairs_arr = array();
$sql_vehicles_arr = array();

if ($r_result -> num_rows > 0){
	while($row = $r_result -> fetch_assoc()){
		array_push($sql_repairs_arr, $row["repair_id"]);
	}
} else {
	echo "0 Entries found in REPAIRS";
}

if ($v_result -> num_rows > 0){
	while($row = $v_result -> fetch_assoc()){
		array_push($sql_vehicles_arr, $row["serial_no"]);
	}
} else {
	echo "0 Entries found in VEHICLES";
}


$repairs_arr = array($sql_repairs_arr[0], $sql_repairs_arr[2], $sql_repairs_arr[2],
$sql_repairs_arr[5], $sql_repairs_arr[7], $sql_repairs_arr[8], $sql_repairs_arr[9],
$sql_repairs_arr[2], $sql_repairs_arr[5], $sql_repairs_arr[8]);

$vehicles_arr = array($sql_vehicles_arr[0], $sql_vehicles_arr[0], $sql_vehicles_arr[1],
$sql_vehicles_arr[3], $sql_vehicles_arr[3], $sql_vehicles_arr[3], $sql_vehicles_arr[4],
$sql_vehicles_arr[5], $sql_vehicles_arr[7], $sql_vehicles_arr[9]);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO repair_history (serial_no, repair_id) ";

	$values = "VALUES ('". $vehicles_arr[$i] ."', '".$repairs_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
