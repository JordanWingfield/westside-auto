﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "CUSTOMERS";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:
$licence_no_arr = array("L120-023", "L120-044", "L131-123", "L002-145", "L012-998",
"L002-556", "L345-660", "L120-465", "L453-111", "L120-344");

$customer_sin_arr = array("111111111", "222222222", "333333333", "444444444", "555555555",
"666666666", "777777777", "888888888", "999999999", "000000000");

$first_name_arr = array("John", "Kate", "William", "Gary", "Emma",
"Marcus", "Marina", "Elissa", "Nick", "Eli");

$last_name_arr = array("Baringham", "Wicket", "Izo", "Terim", "Leftcroft",
"Bigzet", "Singh", "Lamnum", "Ellut", "Manning");

$phone_no_arr = array("000-000-0000", "111-111-1111", "222-222-2222", "333-333-3333", "444-444-4444",
"555-555-5555", "666-666-6666", "777-777-7777", "888-888-8888", "999-999-9999");

$addr_street_no_arr = array(0, 11, 22, 33, 44, 55, 66, 77, 88, 99);

$addr_street_arr = array("Mary Ford Cir", "Ash Rd", "Limeville Ave", "Wiscon Rd", "Kindred Ave",
"Micky Dr", "University Dr", "Barrel Ave", "Apple Rd", "United St");

$addr_city_arr = array("Calgary", "Calgary", "Edmonton", "Lethbridge", "Edmonton",
"Victoria", "Calgary", "Lethbridge", "High River", "Medicine Hat");

$addr_prov_arr = array("AB", "AB", "AB", "AB", "AB", "BC", "AB", "AB", "AB", "AB");

$addr_postal_code_arr = array("AAA-000", "BBB-111", "CCC-333", "DDD-444", "EEE-555",
"FFF-666", "GGG-222", "HHH-777", "III-888", "JJJ-999");

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO Customers (license_no, customer_SIN, first_name, last_name,
	phone_no, addr_street_no, addr_street, addr_city, addr_province, addr_postal_code) ";

	$values = "VALUES ('". $licence_no_arr[$i] ."', '". $customer_sin_arr[$i] ."',
	'". $first_name_arr[$i] ."', '". $last_name_arr[$i] ."', '". $phone_no_arr[$i] ."',
	'". $addr_street_no_arr[$i] ."', '". $addr_street_arr[$i] ."', '". $addr_city_arr[$i] ."',
	'". $addr_prov_arr[$i] ."', '". $addr_postal_code_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
