﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "EMPLOYMENT_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$c_result = $conn->query("SELECT customer_id FROM Customers");
$e_result = $conn->query("SELECT employment_id FROM Employment");

$sql_customer_arr = array();
$sql_employment_arr = array();

if ($c_result -> num_rows > 0){
	while($row = $c_result -> fetch_assoc()){
		array_push($sql_customer_arr, $row["customer_id"]);
	}
} else {
	echo "0 Entries found in CUSTOMERS";
}

if ($e_result -> num_rows > 0){
	while($row = $e_result -> fetch_assoc()){
		array_push($sql_employment_arr, $row["employment_id"]);
	}
} else {
	echo "0 Entries found in EMPLOYMENT";
}


$customer_arr = array($sql_customer_arr[0], $sql_customer_arr[0], $sql_customer_arr[1],
$sql_customer_arr[1], $sql_customer_arr[2], $sql_customer_arr[2], $sql_customer_arr[3],
$sql_customer_arr[3], $sql_customer_arr[3], $sql_customer_arr[4], $sql_customer_arr[4],
$sql_customer_arr[5], $sql_customer_arr[5], $sql_customer_arr[6], $sql_customer_arr[7],
$sql_customer_arr[7], $sql_customer_arr[8], $sql_customer_arr[8], $sql_customer_arr[9]);

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 19;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO employment_history (customer_id, employment_id) ";

	$values = "VALUES ('". $customer_arr[$i] ."', '".$sql_employment_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
