﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "VEHICLES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:

$serial_no_arr = array(110000,110011,110022,110033,110044,110055,110066,110077,110088,110099);

$make_arr = array("Acura", "Ford", "GMC", "Jeep", "Honda", "Toyota", "Ford", "Nissan", "Dodge", "Mitsubishi");

$model_arr = array("EL", "Fiesta", "Sierra", "Grand Cherokee", "Civic", "Camery", "F-150", "Sentra", "Dart", "Lancer");

$year_arr = array(2003, 2006, 2013, 2010, 2004, 2011, 2012, 2009, 2000, 2011);

$style_arr = array("Sedan", "Hatch", "Pick-up", "Van", "Sedan", "Sedan", "Truck", "Coupe", "Hatch", "Coupe");

$miles_arr = array(92000,100000,40000,280000,120000,200000,140000,78000,108000,123000);

$ext_colour_arr = array("Blue", "Green", "Black", "Red", "Grey", "Yellow", "Purple", "Orange", "Magenta", "Pink");

$int_colour_arr = array("Grey", "Beige", "Black", "Pink", "Black", "Beige", "Grey", "Grey", "Black", "Beige");

$vehicle_cond_arr = array("New","Used","New","New","New","Used","Used","New","Used","New");

$book_price_arr = array(4500,5000,20000,10000,2000,3400,5400,6000,3600,1200);

$buy_price_arr = array(2300,3500,17500,9000,500,3000,3090,4600,3000,200);
// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO vehicles (serial_no, make, model, year, style, miles, ext_colour, int_colour, vehicle_condition, book_price, buy_price) ";

	$values = "VALUES ('". $serial_no_arr[$i] ."', '". $make_arr[$i] ."', '". $model_arr[$i] ."',
	'". $year_arr[$i] ."', '".$style_arr[$i]."', '".$miles_arr[$i]."', '".$ext_colour_arr[$i]."', '".$int_colour_arr[$i]."', '".$vehicle_cond_arr[$i]."', '".$book_price_arr[$i]."', '".$buy_price_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
