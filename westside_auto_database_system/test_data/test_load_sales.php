﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "SALES";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:

$total_due_arr = array(10000,6000,3500,4200,5600,6300,7000,4900,2000,6500);

$down_payment_arr = array(1000,2000,500,800,1100,1500,2000,1200,2000,2300);

$finance_amount_arr = array(400,200,250,200,500,400,300,350,0,700);

$sale_date_arr = array("2016-02-28","2017-06-23","2015-12-27","2017-05-28","2015-05-06","2016-08-12","2015-11-17","2017-08-16","2015-01-13","2016-04-07");

// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 10;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO sales (total_due, down_payment, finance_amount, sale_date) ";

	$values = "VALUES ('". $total_due_arr[$i] ."', '". $down_payment_arr[$i] ."',
	'". $finance_amount_arr[$i] ."', '".$sale_date_arr[$i]."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
