﻿<?php
//-----------------------------------------------------
// Table test data creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-26-18
//-----------------------------------------------------

require_once("sql_query_lib.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "EMPLOYMENT";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Test Data:

$start_date_arr = array("2014-04-12", "2014-06-18", "2014-03-22", "2014-04-17",
"2015-05-11", "2015-01-01", "2015-06-23", "2015-09-19", "2016-10-10",
"2016-07-20", "2017-07-12", "2017-04-10", "2017-09-12", "2017-02-10",
"2018-01-12", "2018-02-12", "2018-03-12", "2018-03-20", "2018-03-22",);

$employer_arr = array("Mazda", "Sunlife Financial", "Co-op", "Safeway", "Costco",
"Mazda", "Mazda", "Co-op", "Tim Hortons", "Co-op",
"Sunlife Financial", "Scotia Bank", "Scotia Bank", "RBC", "Safeway",
"Co-op", "RBC", "Sunlife Financial", "Scotia Bank", "Costco");

$title_arry = array("Sales Associate", "Financial Advisor", "Associate", "Associate", "Bagger",
"Sales Associate", "Manager of Sales", "Associate", "Manager", "District Manager",
"Financial Advisor", "Teller", "Teller", "District Manager", "Bagger",
"Baker", "Financial Advisor", "Sanitation Officer", "Teller", "Baker");

$supervisor_arr = array("John Johnston", "Mary Maryland", "Mike Wisowsky", "Abbigail Stuntor", "Wince Rowen",
"Biggy Smalls", "Chase Micheals", "Micheal Jordan", "Adrienne Marline", "Wintins Abben",
"Rowen Atkinson", "Rowen Atkinson", "Norman Mithcy", "Vince Row", "Vince Roberts",
"Mary Marins", "Beletrice Cants", "Vernon Morgnrom", "Morgoth Biggerson", "Shelob Longlegs");

$phone_no_arr = array("111-222-3333", "111-222-3334", "111-222-3344", "111-222-34444", "111-222-4444",
"111-333-4444", "111-333-4445", "111-333-4455", "111-333-4555", "111-333-5555",
"111-444-5555", "111-444-5556", "111-444-5566", "111-444-5666", "111-444-6666",
"111-555-6666", "111-444-5557", "111-444-6677", "111-444-5777", "111-444-7777");

$addr_no_arr = array("30", "45", "66", "798", "405", "11", "12", "901", "82", "394",
"123", "234", "345", "432", "65", "890", "987", "77", "342", "43");

$addr_street_arr = array("Marine Dr", "Marine Dr", "Oakland Ave", "Oakland Ave", "Marine Dr",
"Ygdrasil Cir", "Varsity Dr", "Oakland Ave", "Oakland Ave", "Crowsnest Rd",
"Oakland Ave", "Varsity Dr", "Ygdrasil Cir", "Marine Dr", "Crowsnest Rd", 
"Oakland Ave", "Oakland Dr", "Parkplace Cir", "Crowsnest Cir", "Crowsnest Rd");

$addr_prov_arr = array("AB");

$addr_city_arr = array("Highriver", "Highriver", "Calgary", "Calgary", "Highriver",
"Midgar", "Lethbridge", "Calgary", "Calgary", "Lethbridge",
"Calgary", "Lethbridge", "Midgar", "Highriver", "Lethbridge",
"Calgary", "Calgary", "Lethbridge", "Lethbridge", "Lethbridge");

$addr_postal_code_arr = array("AAA-000", "AAA-222", "AAA-433", "AAA-555", "AAA-666",
"BBB-222", "BBB-111", "BBB-333", "BBB-444", "BBB-777",
"CCC-123", "CCC-234", "CCC-345", "CCC-456", "CCC-567",
"DDD-123", "DDD-234", "DDD-345", "DDD-456", "DDD-567");


// Initial Output:
echo "======================================<br>";
echo "Creating data for table: " . $tbname . "<br>";
echo "--------------------------------------<br>";

// Declare SQL query to run: In this case we are creating a table.
$counter = 19;
for($i = 0; $i < $counter; $i++)
{
	$labels = "INSERT INTO Employment (start_date, employer, title,
	supervisor, phone_no, addr) ";

	$values = "VALUES ('". $start_date_arr[$i] ."', '". $employer_arr[$i] ."',
	'". $title_arry[$i] ."', '". $supervisor_arr[$i] ."', '". $phone_no_arr[$i] ."', '". $addr_no_arr[$i] . " " . 
	 $addr_street_arr[$i] . " " . $addr_prov_arr[0] . " " . $addr_city_arr[$i] . " " . $addr_postal_code_arr[$i] ."')";

	$sql = $labels . $values;

	insert_into_table($conn, $sql, $tbname);
}

$conn->close();

// Ending Output:
echo "--------------------------------------<br>";
echo "Finished creating data for table: " . $tbname . "<br>";
echo "======================================<br>";

?>
