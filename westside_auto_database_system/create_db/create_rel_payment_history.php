<?php
//-----------------------------------------------------
// Table creation PHP script
// Written By: Jordan (Via W3Schools)
// 03-22-18
//-----------------------------------------------------

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "westside_auto";

$tbname = "PAYMENT_HISTORY";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Declare SQL query to run: In this case we are creating a table.
$sql = "CREATE TABLE Payment_history ( customer_id int,
payment_no int, 
PRIMARY KEY (customer_id, payment_no),
FOREIGN KEY (payment_no) REFERENCES Payments (payment_no),
FOREIGN KEY (customer_id) REFERENCES Customers (customer_id))";

// Run query via $conn function query(SQL_QUERY), and return success message
// if the table was created successfully.
if ($conn->query($sql) === TRUE) {
  echo "Table " . $tbname . " created successfully.";
} else {
  echo "Error creating table " . $tbname . ": " . $conn->error;
}
echo "<br>";

$conn->close();
?>