# Westside Auto

W e s t s i d e   A u t o   D a t a b a s e   S y s t e m:

University of Lethbridge
Computer Science 3660 (Databases) Project
Creation Date: 03-15-18

Project Members: J. Wingfield, C. Perlette, R. Ozar

The aim of this project is to build the structure of the auto-business &#34;Westside Auto&#34; into a usable client-server model database system.

The core focus of this system utilizes MySQL as the database engine, HTML web code for web page access, and PHP as the interpretive language.